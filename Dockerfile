# ---------------------------------------
# Development stage
# ---------------------------------------
FROM virtual-exhibition.kingone-design.com:8998/kingone/php81-image:1.0 AS development

ENV APP_PATH=/var/www/nginx
WORKDIR /var/www/nginx

EXPOSE 8106

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]

# ---------------------------------------
# Production stage
# ---------------------------------------
FROM virtual-exhibition.kingone-design.com:8998/kingone/php81-image:1.0 AS production

ENV APP_PATH=/var/www/nginx
WORKDIR /var/www/nginx

COPY . $APP_PATH

COPY ./docker/nginx/nginx-default.conf /etc/nginx/http.d/default.conf
COPY ./docker/php/php.ini /usr/local/etc/php/conf.d/php-additional-setting.ini
COPY ./docker/supervisord/supervisord.conf /etc/supervisord.conf

ENV COMPOSER_ALLOW_SUPERUSER=1

RUN composer install
RUN composer install --optimize-autoloader --no-dev \
    && cp .env.example .env
RUN composer update \
    && composer dump-autoload -o \
    && chown www-data:www-data -R $APP_PATH \
    && chmod 775 -R $APP_PATH \
    && chmod o+w -R $APP_PATH/storage
RUN php artisan key:generate \
    && php artisan storage:link
# && php artisan config:cache \
# && php artisan route:cache \
# && php artisan view:cache

RUN apk add yarn \
    && yarn install \
    && yarn build

EXPOSE 80

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]