<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CockpitCategoryContoller;
use App\Http\Controllers\CockpitPostController;
use App\Http\Controllers\DesktopCategoryController;
use App\Http\Controllers\DesktopPostController;
use App\Http\Controllers\Fans3dCategoryController;
use App\Http\Controllers\Fans3dPostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LocaleController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProjectionCategoryController;
use App\Http\Controllers\ProjectionPostController;
use App\Http\Controllers\ScreenPostController;
use App\Http\Controllers\TheaterCategoryController;
use App\Http\Controllers\TheaterPostController;
use App\Http\Controllers\TvwallCategoryController;
use App\Http\Controllers\TvwallPostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Auth::routes();

// Route::middleware(['auth', 'additional.auth'])->group(function () {
//     Route::get('/', [HomeController::class, 'index']);
//     Route::get('/home', [HomeController::class, 'index'])->name('home');
//     Route::resource('/media', MediaController::class);

//     // Route::resource('/post', PostController::class);
//     // Route::resource('/category', CategoryController::class);
//     Route::resource('/users', UserController::class);

//     Route::resource('/locale', LocaleController::class);

//     /**
//      * tvwall
//      */
//     Route::resource('/tvwall_category', TvwallCategoryController::class);
//     Route::post('/tvwall_category_sort', [TvwallCategoryController::class, 'store_sort'])->name('tvwall_category.store_sort');
//     Route::resource('/tvwall_post', TvwallPostController::class);
//     Route::get('/tvwall_standby_video', [TvwallPostController::class, 'standby_video'])->name('tvwall_standby_video.show');
//     Route::post('/tvwall_standby_video', [TvwallPostController::class, 'standby_video'])->name('tvwall_standby_video.update');

//     /**
//      * theater
//      */
//     Route::resource('/theater_category', TheaterCategoryController::class);
//     Route::resource('/theater_post', TheaterPostController::class);
//     Route::get('/theater_standby_video', [TheaterPostController::class, 'standby_video'])->name('theater_standby_video.show');
//     Route::post('/theater_standby_video', [TheaterPostController::class, 'standby_video'])->name('theater_standby_video.update');

//     /**
//      * cockpit
//      */
//     Route::resource('/cockpit_category', CockpitCategoryContoller::class);
//     Route::resource('/cockpit_post', CockpitPostController::class);
//     Route::get('/cockpit_standby_video', [CockpitPostController::class, 'standby_video'])->name('cockpit_standby_video.show');
//     Route::post('/cockpit_standby_video', [CockpitPostController::class, 'standby_video'])->name('cockpit_standby_video.update');

//     /**
//      * desktop
//      */
//     Route::resource('/desktop_category', DesktopCategoryController::class);
//     Route::post('/desktop_category_sort', [DesktopCategoryController::class, 'store_sort'])->name('desktop_category.store_sort');
//     Route::resource('/desktop_post', DesktopPostController::class);
//     Route::get('/desktop_standby_video', [DesktopPostController::class, 'standby_video'])->name('desktop_standby_video.show');
//     Route::post('/desktop_standby_video', [DesktopPostController::class, 'standby_video'])->name('desktop_standby_video.update');

//     /**
//      * projection
//      */
//     Route::resource('/projection_category', ProjectionCategoryController::class);
//     Route::post('/projection_category_sort', [ProjectionCategoryController::class, 'store_sort'])->name('projection_category.store_sort');
//     Route::resource('/projection_post', ProjectionPostController::class);

//     /**
//      * screen
//      */
//     Route::resource('/screen_post', ScreenPostController::class);
//     Route::get('/screen_standby_video', [ScreenPostController::class, 'standby_video'])->name('screen_standby_video.show');
//     Route::post('/screen_standby_video', [ScreenPostController::class, 'standby_video'])->name('screen_standby_video.update');

//     /**
//      * fans3d
//      */
//     Route::resource('/fans3d_category', Fans3dCategoryController::class);
//     Route::post('/fans3d_category_sort', [Fans3dCategoryController::class, 'store_sort'])->name('fans3d_category.store_sort');
//     Route::resource('/fans3d_post', Fans3dPostController::class);
//     Route::get('/fans3d_standby_video', [Fans3dPostController::class, 'standby_video'])->name('fans3d_standby_video.show');
//     Route::post('/fans3d_standby_video', [Fans3dPostController::class, 'standby_video'])->name('fans3d_standby_video.update');
// });
