<?php

use App\Http\Controllers\Api\AwardController;
use App\Http\Controllers\Api\AwardPostController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CockpitController;
use App\Http\Controllers\Api\DesktopController;
use App\Http\Controllers\Api\Fans3dController;
use App\Http\Controllers\Api\MediaController as ApiMediaController;
use App\Http\Controllers\Api\PatentController;
use App\Http\Controllers\Api\PatentPostController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\ProjectionController;
use App\Http\Controllers\Api\ScreenController;
use App\Http\Controllers\Api\SystemController;
use App\Http\Controllers\Api\TheaterController;
use App\Http\Controllers\Api\TvwallController;
use App\Http\Controllers\Api\VideoController;
use App\Http\Controllers\Api\VideoPostController;
use App\Http\Controllers\TvwallCategoryController;
use App\Http\Controllers\TvwallPostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::resource('/category', CategoryController::class)->except('create', 'edit');
Route::resource('/post', PostController::class)->except('create', 'edit');



/**
 * tvwall
 */
Route::get('/tvwall', [TvwallController::class, 'index']);
/**
 * theater
 */
Route::get('/theater', [TheaterController::class, 'index']);
/**
 * desktop
 */
Route::get('/desktop', [DesktopController::class, 'index']);
/**
 * cockpit
 */
Route::get('/cockpit', [CockpitController::class, 'index']);
/**
 * projection
 */
Route::get('/projection', [ProjectionController::class, 'index']);
/**
 * screen
 */
Route::get('/screen', [ScreenController::class, 'index']);
Route::post('/screen/{post_id}', [ScreenController::class, 'update']);

/**
 * fans3d
 */
Route::get('/fans3d', [Fans3dController::class, 'index']);

// Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

/**
 * forntend video
 */
Route::get('/video_category', [VideoController::class, 'index']);

/**
 * forntend patent
 */
Route::get('/patent_category', [PatentController::class, 'index']);

/**
 * forntend award
 */
Route::get('/award_category', [AwardController::class, 'index']);

/**
 *  sort
 */
Route::post('/category/sort', [CategoryController::class, 'sort']);
Route::post('/category/{category}/post/sort', [CategoryController::class, 'postSort']);

Route::prefix('admin')->group(function () {
/**
 * video
 */
Route::resource('/video_category', VideoController::class);
Route::resource('/video_post', VideoPostController::class);


/**
 * patent
 */

 Route::resource('/patent_category', PatentController::class);
 Route::resource('/patent_post', PatentPostController::class);

/**
 * award
 */

Route::resource('/award_category', AwardController::class);
Route::resource('/award_post', AwardPostController::class);


//影片上傳
Route::resource('/media', ApiMediaController::class)->except('create', 'edit');

//媒體排序
Route::post('/post/{post}/media/sort', [PostController::class, 'postMediaSort']);
Route::post('/category/{category}/media/sort', [CategoryController::class, 'categoryMediaSort']);

//語言頁面
Route::get('/system/locale', [SystemController::class, 'systemLocale'])->name('system.locale');
});

// });
