# Docker

## build

docker build -t foxconn_showroom:latest .

docker build --no-cache -t foxconn_showroom:latest .

## run

### develpoyement

docker compose up -d 

### production

docker run -d --name foxconn_showroom -p 18091:80 --network server foxconn_showroom:latest

docker network connect data foxconn_showroom
