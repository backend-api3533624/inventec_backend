<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AdditionalAuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // 進行額外的檢查
        if (Auth::check() && Auth::user()) {
            $user = Auth::user();
            if ($user->status == 0) {
                Auth::logout();
                abort('403', '請聯繫管理員');
            }
            $user_roles = $user->getRoleNames()->toArray();
            if (empty($user_roles)) {
                Auth::logout();
                abort('403', '請聯繫管理員');

                // return redirect('/login')->with('error', 'Unauthorized Access');
            }

            // 通過檢查，繼續執行請求
            return $next($request);
        }
    }
}
