<?php

namespace App\Http\Resources;

use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        // return parent::toArray($request);

        $locales = Locale::where('status', 1)->get();
        $content = [];
        foreach ($locales as $key => $locale) {
            $content[$locale->code] = $this->getTranslation('content', $locale->code, 0);
        }
        $description = [];
        foreach ($locales as $key => $locale) {
            $description[$locale->code] = $this->getTranslation('description', $locale->code, 0);
        }
        $thumbnail = [];
        foreach ($locales as $key => $locale) {
            $url = $this->getTranslation('thumbnail', $locale->code, 0);
            if (! empty($url)) {
                $url = asset('storage/'.$url);
            }
            $thumbnail[$locale->code] = $url;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'visible' => $this->status,
            'removeable' => $this->removeable,
            'title' => $content,
            'content' => $description,
            'thumbnail' => $thumbnail,
            'order' => $this->sort,
            'parent_id' => ($this->parent_id > 0) ? $this->parent_id : 0,
            'posts' => new PostCollection($this->posts),
            'medias' => new MediaCollection($this->medias),
            'children' => new CategoryCollection($this->children),
            'metas' => [],
            'updated_at' => $this->updated_at,
        ];
    }
}
