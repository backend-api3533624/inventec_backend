<?php

namespace App\Http\Resources;

use App\Models\Locale;
use App\Models\Postmeta;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        // $metas = [
        //     'screen_position' => 'left',
        // ];
        $metas = [];
        $post_metas = Postmeta::where('post_id', $this->id)->get();
        if (! empty($post_metas)) {
            foreach ($post_metas as $key => $value) {
                $metas[$value['meta_key']] = $value['meta_value'];
            }
        }

        $locales = Locale::where('status', 1)->get();
        $title = [];
        foreach ($locales as $key => $locale) {
            $title[$locale->code] = $this->getTranslation('title', $locale->code, 0);
        }
        $content = [];
        foreach ($locales as $key => $locale) {
            $content[$locale->code] = $this->getTranslation('content', $locale->code, 0);
        }
        $description = [];
        foreach ($locales as $key => $locale) {
            $description[$locale->code] = $this->getTranslation('description', $locale->code, 0);
        }

        $thumbnail = [];
        foreach ($locales as $key => $locale) {
            $url = $this->getTranslation('thumbnail', $locale->code, 0);
            if (! empty($url)) {
                $url = asset('storage/'.$url);
            }
            $thumbnail[$locale->code] = $url;
        }

        $medias = [];
        foreach ($this->medias as $key => $value) {
            // if ($value->pivot->related_locale == 'ar_xa') {
            //     continue;
            // }
            $medias[] = $value;
        }

        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'visible' => $this->status,
            'removeable' => $this->removeable,
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'thumbnail' => $thumbnail,
            'order' => $this->menu_order,
            'medias' => new MediaCollection($medias),
            'json_meta' => json_encode($metas),
            'updated_at' => $this->updated_at,
        ];
    }
}
