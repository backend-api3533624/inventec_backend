<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StandByVideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $mime_type = null;
        $setting_value = collect(json_decode($this->setting_value, true));
        if (! empty($setting_value)) {
            if (isset($setting_value['type'])) {
                $mime_type = $setting_value['type'];
            }
        }
        $filepath = null;
        if (isset($setting_value['file_path'])) {
            $filepath = $setting_value['file_path'];
        }

        return [
            'id' => $this->id,
            'type' => $mime_type,
            'url' => asset('storage/'.$filepath),
            'description' => $this->description,
            'updated_at' => $this->updated_at,
        ];
    }
}
