<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        // return parent::toArray($request);

        return [
            'id' => $this->id,
            'locale' => (! empty($this->pivot)) ? $this->pivot->related_locale : null,
            'name' => $this->name,
            'type' => $this->type,
            'url' => $this->file_path,
            'selectBy' => $this->selectBy,
            'order' => (! empty($this->pivot)) ? $this->pivot->item_sort : 0,
            'thumbnail' => $this->thumbnail,
            'updated_at' => $this->updated_at,
        ];
    }
}
