<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $category_type = 'tvwall';

    private $locale = 'zh_cht';

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $categories = Category::whereNull('parent_id')
            ->with('children')
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate(10);

        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $locale = $this->locale;

        return view('categories.create', compact('locale'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->type = $this->category_type;
        $category->sort = $request->sort;
        $category->parent_id = $request->parent_id;
        $category->setTranslation('content', $this->locale, $request->content);
        $category->setTranslation('description', $this->locale, $request->description);

        $category->save();

        return redirect()->route('category.index')->with('success', '類別已建立');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category, Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $locale = $this->locale;

        return view('categories.edit', compact('category', 'locale'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $category)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        $category->name = $request->name;
        $category->sort = $request->sort;
        $category->parent_id = $request->parent_id;
        $category->setTranslation('content', $this->locale, $request->content);
        $category->setTranslation('description', $this->locale, $request->description);

        $category->update();

        return redirect()->route('category.index')->with('success', '類別已建立');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        // 檢查類別是否有子類別，如果有，防止刪除
        if ($category->children()->count() > 0) {
            return redirect()->back()->with('error', '無法刪除，該類別下有子類別存在。');
        }

        $category->delete();

        return redirect()->back()->with('success', '類別已刪除');
    }
}
