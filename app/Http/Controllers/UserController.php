<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $user_roles = Auth::user()->getRoleNames()->toArray();
        if (! in_array('admin', $user_roles)) {
            return abort(403, '無權限');
        }
        $users = User::when($request->filter, function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->filter.'%');
        })
            ->orderBy('id', 'ASC')
            ->paginate(10);

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user_roles = Auth::user()->getRoleNames()->toArray();
        if (! in_array('admin', $user_roles)) {
            return abort(403, '無權限');
        }
        $roles = Role::all();

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
        ]);
        $user = new User();
        $user->name = $request->input('username');
        $user->status = (int) $request->input('status');
        $user->password = bcrypt($request->input('password'));
        $user->email = $request->input('email');
        $user->save();

        Log::info(json_encode([
            'action user id' => Auth::id(),
            'create user' => $user,
        ]));

        // 角色
        if (! empty($request->input('roles'))) {
            $user->syncRoles($request->input('roles'));

            Log::info(json_encode([
                'action user id' => Auth::id(),
                'create user role' => $request->input('roles'),
            ]));
        }

        return redirect()->route('users.edit', [$user->id])
            ->with('success', '新增帳號成功');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user_roles = Auth::user()->getRoleNames()->toArray();
        if (! in_array('admin', $user_roles)) {
            return abort(403, '無權限');
        }
        $user = User::find($id);
        if (empty($user)) {
            return abort(404, '無此帳號');
        }

        return view('users.edit', [
            'user' => $user,
            'roles' => Role::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'username' => 'required|max:255',
            'email' => 'required',
            'password' => '',
        ]);
        $user = User::find($id);
        if (! empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        if ($request->status != $user->status) {
            $user->status = (int) $request->status;
        }

        $user->save();

        // 角色
        if (! empty($request->input('roles'))) {
            $user->syncRoles($request->roles);
        } else {
            $user->syncRoles([]);
        }

        return redirect()->route('users.edit', $id)
            ->with('success', '更新帳號成功！');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
