<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\PostResource;
use App\Http\Resources\StandByVideoResource;
use App\Models\Category;
use App\Models\Post;
use App\Models\Postmeta;
use App\Models\SystemSetting;
use App\Traits\ApiResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Type\Integer;

class ScreenController extends Controller
{
    use ApiResponseTrait;

    private $category_type = 'screen_category';

    private $locale = 'zh_cht';

    private $parent_category = false;

    public function lastEdit()
    {
        $data = [
            'last_edit' => Carbon::now(),
        ];

        return $this->apiResponse($data, 200);
    }

    public function index(Request $request)
    {
        $last_edit_timestamp = SystemSetting::where('setting_key', 'screen_last_edit_timestamp')->value('setting_value');
        if (empty($last_edit_timestamp)) {
            $last_edit_timestamp = Carbon::now();
        } else {
            $last_edit_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $last_edit_timestamp);
        }

        $standby_video = SystemSetting::where('setting_key', 'screen_standby_video')->first();

        $categories = Category::whereNull('parent_id')
            ->with([
                'posts' => function ($query) {
                    $query->where('posts.status', 1);
                },
            ])
            ->with('children')
            ->where('status', 1)
            ->where('type', $this->category_type)
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'ASC')
            ->get();

        foreach ($categories as $category) {
            $category->posts->load('metas', 'medias');
        }
        $data = [
            'updated_at' => $last_edit_timestamp,
            'standby_asset' => new StandByVideoResource($standby_video),
            'categories' => new CategoryCollection($categories),
        ];

        return $this->apiResponse($data, 200);
    }

    public function update(Request $request, Int $post_id)
    {
        $post = Post::find($post_id);

        Postmeta::updateOrCreate([
            'post_id' => $post->id,
            'meta_key' => 'screen_json',
        ], [
            'meta_value' => $request->screen_json,
        ]);
        
        $post->load('metas');

        return $this->apiResponse(new PostResource($post));
    }
}
