<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function postMediaSort(Request $request, Post $post)
    {
        $item_sort = explode(',', $request->item_sort);
        foreach ($item_sort as $index => $media_id) {
            Attachment::where('media_id', $media_id)
                ->where('related_id', $post->id)
                ->where('related_type', 'App\Models\Post')
                ->update([
                    'item_sort' => $index + 1,
                ]);
        }

        return response()->json([
            'status' => 200,
            'data' => [],
        ], 200);
    }
}
