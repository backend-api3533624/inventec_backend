<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Media;
use App\Models\SystemSetting;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    use ApiResponseTrait;

    private $category_type = 'video_category';

    private $locale = 'zh_cht';

    private $parent_category = false;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $page = $request->input('page', 1);
        $per_page = $request->input('per_page', 10);
        
        $parent_category = $this->parent_category;
        $category_type = $this->category_type;
        //獲取category為第一層的資料，而不是自關聯的第二層
        $categories = Category::whereNull('parent_id')
            ->where('type', $this->category_type)
            ->with([
                'posts' => function ($query) {
                    $query->where('posts.status', 1);
                },
                'medias',
                'children'
            ])
            //filter為以後要關鍵字搜尋的彈性
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate($per_page, '*' , 'page', $page);
        
        $data = compact('category_type', 'categories', 'parent_category');

        return $this->apiResponse($data, 200);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // $parent_category = $this->parent_category;
        // $route = $this->category_type;
        // $category_type = $this->category_type;
        // $locale = $this->locale;

        // return view('categories.create', compact('category_type', 'locale', 'parent_category'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        //先去找是否有語言變更
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }

        $validatedData = $request->validate([
            'status' => 'required|boolean',
            'name' => 'required|max:255',
            // 'content' => 'required|max:255',
            'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);
        $category = new Category();
        $category->status = $request->status;
        $category->type = $this->category_type;
        $category->setTranslation('name', $this->locale, $request->input('name'));
        $category->sort = $request->input('sort');
        $category->parent_id = $request->input('parent_id');
        $category->setTranslation('content', $this->locale, $request->input('content'));
        $category->setTranslation('description', $this->locale, $request->input('description'));

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = $request->input('thumbnail_name');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');
                $absolute_path_thumbnail = env('ASSET_URL') . '/' . $path_thumbnail;

                $media = new Media();
                $media->name = $thumbnailName ?? $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $absolute_path_thumbnail;
                $media->save();
                $category->setTranslation('thumbnail', $this->locale, $absolute_path_thumbnail);
            }
        }

        $category->save();

        SystemSetting::updateOrCreate([
            'setting_key' => 'video_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return response()->json(['message' => 'Saved successfully'], 200);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Category $tvwall_category)
    {
        // // $user_roles = Auth::user()->getRoleNames()->toArray();
        // // dd($user_roles);
        // $parent_category = $this->parent_category;
        // $category = $tvwall_category;
        // $category_type = $this->category_type;
        // if ($request->has('locale')) {
        //     $this->locale = $request->input('locale');
        // }
        // $locale = $this->locale;

        // $posts = $category->posts;
        // $category->load(['medias' => function ($query) {
        //     $query->where('related_locale', $this->locale);
        // }]);

        // foreach ($category->medias as $key => $media) {
        //     $media->setAttribute('url', $media->file_path);
        // }

        // $medias = $category->medias;

        // return view('categories.edit', compact(
        //     'category_type',
        //     'locale',
        //     'category',
        //     'parent_category',
        //     'posts',
        //     'medias',
        // ));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $video_category)
    {

        //用url中後最後面的id值直接去查找資料庫返回實例為$video_catergory
        $category = $video_category;
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'status' => 'required|boolean',
            'name' => 'required|max:255',
            // 'content' => 'required|max:255',
            'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        $category->status = $request->status;
        $category->setTranslation('name', $this->locale, $request->input('name'));
        $category->sort = $request->input('sort');
        $category->parent_id = $request->input('parent_id');
        $category->setTranslation('content', $this->locale, $request->input('content'));
        $category->setTranslation('description', $this->locale, $request->input('description'));

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = $request->input('thumbnail_name');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');
                $absolute_path_thumbnail = env('ASSET_URL') . '/' . $path_thumbnail;

                $media = new Media();
                $media->name = $thumbnailName ?? $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $absolute_path_thumbnail;
                $media->save();
                $category->setTranslation('thumbnail', $this->locale, $absolute_path_thumbnail);
            }
        }
        
        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $category->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $category->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $value) {
                $category->medias()->attach($value, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }
        $category->save();

        SystemSetting::updateOrCreate([
            'setting_key' => 'video_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return response()->json(['message' => 'Updated successfully!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $video_category)
    {
        $category = $video_category;
        
        // 檢查類別是否有子類別，如果有，防止刪除
        if ($category->children()->count() > 0) {
            return response()->json(['error' => 'Cannot delete, subcategories exist under this category!'], 403);
        }
        
        $category->delete();

        SystemSetting::updateOrCreate([
            'setting_key' => 'video_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        return response()->json(['message' => 'Deleted successfully!'], 200);
    }

    public function store_sort(Request $request)
    {
        // return redirect()->back()->with('success', '順序已更新');
    }
}
