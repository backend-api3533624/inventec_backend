<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Category;
use App\Models\CategoryRelationship;
use App\Models\Post;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ApiResponseTrait;

    private $locale = 'zh_cht';

    public function categoryMediaSort(Request $request, Category $category)
    {
        $item_sort = explode(',', $request->item_sort);
        foreach ($item_sort as $index => $media_id) {
            Attachment::where('media_id', $media_id)
                ->where('related_id', $category->id)
                ->where('related_type', 'App\Models\Category')
                ->update([
                    'item_sort' => $index,
                ]);
        }

        return response()->json([
            'status' => 200,
            'data' => [],
        ], 200);
    }

    public function sort(Request $request)
    {
        $cate_sort = explode(',', $request->cate_sort);

        foreach ($cate_sort as $index => $cate_id) {
            $category = Category::where('id', $cate_id)->first();
            $category->sort = $index;
            $category->save();
        }

        return response()->json(['message' => "Sorted successfully!",], 200);
    }

    public function postSort(Request $request, Category $category)
    {
        $item_sort = explode(',', $request->item_sort);
        foreach ($item_sort as $index => $post_id) {
            CategoryRelationship::where('category_id', $category->id)
                ->where('related_id', $post_id)
                ->where('related_type', 'App\Models\Post')
                ->update([
                    'item_sort' => $index + 1,
                ]);
        }

        if ($request->has('type')) {
            switch ($request->type) {
                case 'screen_post':
                    foreach ($item_sort as $index => $post_id) {
                        Post::where('id', $post_id)->update([
                            'menu_order' => $index,
                        ]);
                    }
                    break;

                default:
                    // code...
                    break;
            }
        }

        return response()->json([
            'status' => 200,
            'data' => [],
        ], 200);
    }

    public function index(Request $request)
    {
        $categories = Category::whereNull('parent_id')
            ->with('children')
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate(10);

        return $this->apiResponse($categories, 200);
    }

    public function store(Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->type = $request->category_type;
        $category->sort = $request->sort;
        $category->parent_id = $request->parent_id;
        $category->setTranslation('content', $this->locale, $request->content);
        $category->setTranslation('description', $this->locale, $request->description);

        $category->save();

        return $this->apiResponse(
            ['id' => $category->id], 200);
    }

    public function update(Request $request, Category $category)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        $category->name = $request->name;
        $category->sort = $request->sort;
        $category->parent_id = $request->parent_id;
        $category->setTranslation('content', $this->locale, $request->content);
        $category->setTranslation('description', $this->locale, $request->description);

        $category->update();

        return $this->apiResponse(null, 200);
    }
}
