<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MediaCollection;
use App\Models\Attachment;
use App\Models\Media;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{
    use ApiResponseTrait;
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {   
        $page = $request->input('page', 1);
        $per_page = $request->input('per_page', 10);

        $medias = Media::when($request->filter, function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->filter.'%');
        })->paginate($per_page, '*' , 'page', $page);

        foreach ($medias as $media) {
            $selectBy = false;
            if ($request->has('post_id')) {
                $post_id = $request->post_id;
                $selected = Attachment::where('media_id', $media->id)
                    ->where('related_id', $post_id)
                    ->where('related_type', 'App\Models\Post')
                    ->where('related_locale', $request->locael)
                    ->pluck('media_id')->toArray();
                if (in_array($media->id, $selected)) {
                    $selectBy = true;
                }
            }
            if ($request->has('category_id')) {
                $category_id = $request->category_id;
                $selected = Attachment::where('media_id', $media->id)
                    ->where('related_id', $category_id)
                    ->where('related_type', 'App\Models\Category')
                    ->where('related_locale', $request->locael)
                    ->pluck('media_id')->toArray();
                if (in_array($media->id, $selected)) {
                    $selectBy = true;
                }
            }
            $media->setAttribute('selectBy', $selectBy);
        }

        $mediaCollection = new MediaCollection($medias);

        return $this->apiResponse($mediaCollection, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'media_file' => 'required|file',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors(),
            ], 422);
        }
        $file = $request->file('media_file');
        $path_file = $file->store('media', 'public');

        $path_thumbnail = '';
        if ($request->has('media_thumbnail')) {
            $thumbnail = $request->file('media_thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');
            }
        }

        $media = new Media();
        $media->name = $request->input('media_name') ?? $file->getClientOriginalName();
        $media->file_path = $path_file;
        $media->type = $file->getMimeType();
        $media->thumbnail = $path_thumbnail;
        $media->save();

        return response()->json([
            'media' => $media->toArray(),

        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
