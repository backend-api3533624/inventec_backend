<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Media;
use App\Models\Post;
use App\Models\SystemSetting;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class VideoPostController extends Controller
{
    use ApiResponseTrait;

    private $post_type = 'video_post';

    private $locale = 'zh_cht';

    private $parent_category = false;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $page = $request->input('page', 1);
        $per_page = $request->input('per_page', 10);

        $post_type = $this->post_type;
        $posts = Post::where('type', $this->post_type)
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->with('categories', 'medias')
            ->orderBy('menu_order', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate($per_page, '*' , 'page', $page);
        
        $data = compact('post_type', 'posts');

        return $this->apiResponse($data, 200);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request, Post $video_post)
    {
        // $post = $video_post;
        // $post_type = $this->post_type;
        // $locale = $this->locale;
        // $categories = Category::where('type', 'tvwall_category')->where('removeable', 1)->orderBy('sort', 'asc')->orderBy('id', 'asc')->get();

        // $post->load(['medias' => function ($query) {
        //     $query->where('related_locale', $this->locale);
        // }, 'categories']);

        // $mediaFiles = Media::when($request->filter, function ($query) use ($request) {
        //     $query->where('name', 'LIKE', '%'.$request->filter.'%');
        // })->paginate(20);

        // return view('posts/create', compact('route', 'locale', 'post_type', 'categories', 'mediaFiles', 'post'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'status' => 'required|boolean',
            'name' => 'required|max:255',
            'description' => 'required',
            // 'title' => 'required|max:255',
            // 'content' => 'required',
        ]);

        $post = new Post();
        $post->setTranslation('name', $this->locale, $request->name);
        $post->setTranslation('title', $this->locale, $request->title);
        $post->setTranslation('description', $this->locale, $request->description);
        $post->setTranslation('content', $this->locale, $request->content);
        $post->status = $request->status;
        $post->removeable = true;
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->type = $this->post_type;
        $post->save();

        if ($request->has('category')) {
            $categories_ids = [];
            foreach ($request->category as $key => $value) {
                if (! empty($value)) {
                    // $categories_ids[$value] = ['item_sort' => 999];
                    $categories_ids[] = $value;
                }
            }
            $post->categories()->sync($categories_ids);
        }

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = $request->input('thumbnail_name');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');
                $absolute_path_thumbnail = env('ASSET_URL') . '/' . $path_thumbnail;

                $media = new Media();
                $media->name = $thumbnailName ?? $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $absolute_path_thumbnail;
                $media->save();
                $post->setTranslation('thumbnail', $this->locale, $absolute_path_thumbnail);
                $post->save();
            }
        }

        // media_ids[]
        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $post->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $post->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $meida_id) {
                $post->medias()->attach($meida_id, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }

        SystemSetting::updateOrCreate([
            'setting_key' => 'video_post_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return response()->json(['message' => 'Saved successfully'], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $video_post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Post $video_post)
    {
        // if ($request->has('locale')) {
        //     $this->locale = $request->input('locale');
        // }
        // $post = $video_post;
        // $post_type = $this->post_type;
        // $route = $this->post_type;
        // $locale = $this->locale;

        // $post->load(['medias' => function ($query) {
        //     $query->where('related_locale', $this->locale);
        // }, 'categories']);
        // $mediaFiles = Media::when($request->filter, function ($query) use ($request) {
        //     $query->where('name', 'LIKE', '%'.$request->filter.'%');
        // })->paginate(20);

        // $categories = Category::where('type', 'tvwall_category')->where('removeable', 1)->orderBy('sort', 'asc')->orderBy('id', 'asc')->get();

        // return view('posts/edit', compact('route', 'locale', 'post_type', 'post', 'categories', 'mediaFiles'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $video_post)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            'status' => 'required|boolean',
            'name' => 'required|max:255',
            'description' => 'required',
            // 'title' => 'required|max:255',
            // 'content' => 'required',
        ]);

        $post = $video_post;
        $post->setTranslation('name', $this->locale, $request->name);
        $post->setTranslation('title', $this->locale, $request->title);
        $post->setTranslation('description', $this->locale, $request->description);
        $post->setTranslation('content', $this->locale, $request->content);
        $post->status = $request->status ? 1 : 0;
        $post->removeable = true;
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->type = $this->post_type;
        $post->save();

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = $request->input('thumbnail_name');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');
                $absolute_path_thumbnail = env('ASSET_URL') . '/' . $path_thumbnail;

                $media = new Media();
                $media->name = $thumbnailName ?? $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $absolute_path_thumbnail;
                $media->save();
                $post->setTranslation('thumbnail', $this->locale, $absolute_path_thumbnail);
                $post->save();
            }
        }

        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $post->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $post->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $value) {
                $post->medias()->attach($value, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }

        if ($request->has('category')) {
            $categories_ids = [];
            foreach ($request->category as $key => $value) {
                if (! empty($value)) {
                    $categories_ids[] = $value;
                }
            }
            $post->categories()->sync($categories_ids);
            // dd($categories_ids);
        }

        SystemSetting::updateOrCreate([
            'setting_key' => 'video_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return response()->json(['message' => 'Updated successfully!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $video_post)
    {
        $post = $video_post;

        $post->medias()->sync([]);

        $post->delete();

        SystemSetting::updateOrCreate([
            'setting_key' => 'video_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return response()->json(['message' => 'Deleted successfully!'], 200);
    }
}