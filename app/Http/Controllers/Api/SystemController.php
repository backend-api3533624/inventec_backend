<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Locale;
use App\Traits\ApiResponseTrait;

class SystemController extends Controller
{
    use ApiResponseTrait;

    public function systemLocale()
    {
        $system_locale = Locale::all();

        return $this->apiResponse($system_locale);
    }
}
