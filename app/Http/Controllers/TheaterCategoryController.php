<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Media;
use App\Models\SystemSetting;
use Illuminate\Http\Request;

class TheaterCategoryController extends Controller
{
    private $category_type = 'theater_category';

    private $locale = 'zh_cht';

    private $parent_category = false;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $parent_category = $this->parent_category;
        $route = $this->category_type;
        $category_type = $this->category_type;
        $categories = Category::whereNull('parent_id')
            ->where('type', $this->category_type)
            ->with('children')
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'ASC')
            ->get();

        return view('categories.index', compact(
            'route',
            'category_type',
            'categories',
            'parent_category'
        ));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $parent_category = $this->parent_category;
        $route = $this->category_type;
        $category_type = $this->category_type;
        $locale = $this->locale;

        return view('categories.create', compact('category_type', 'locale', 'parent_category'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }

        $validatedData = $request->validate([
            // 'name' => 'required|max:255',
            'content' => 'required|max:255',
            // 'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);
        $category = new Category();
        $category->status = $request->status ? 1 : 0;
        $category->type = $this->category_type;
        $category->name = '';
        $category->sort = $request->sort;
        $category->parent_id = $request->parent_id;
        $category->setTranslation('content', $this->locale, $request->content);
        $category->setTranslation('description', $this->locale, '');

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');

                $media = new Media();
                $media->name = $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $path_thumbnail;
                $media->save();
                $category->setTranslation('thumbnail', $this->locale, $path_thumbnail);
            }
        }

        $category->save();

        SystemSetting::updateOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->route($this->category_type.'.index')->with('success', '類別已建立');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Category $theater_category)
    {
        $parent_category = $this->parent_category;
        $category = $theater_category;
        $category_type = $this->category_type;
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $locale = $this->locale;

        $posts = $category->posts;
        $category->load(['medias' => function ($query) {
            $query->where('related_locale', $this->locale);
        }]);

        foreach ($category->medias as $key => $media) {
            $media->setAttribute('url', $media->file_path);
        }

        $medias = $category->medias;

        return view('categories.edit', compact(
            'category_type',
            'locale',
            'category',
            'parent_category',
            'posts',
            'medias',
        ));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $theater_category)
    {
        $category = $theater_category;
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            // 'name' => 'required|max:255',
            'content' => 'required|max:255',
            // 'description' => 'required',
            'sort' => 'required|numeric',
            'parent_id' => 'nullable|exists:categories,id',
        ]);
        $category->status = $request->status ? 1 : 0;
        $category->name = '';
        $category->sort = $request->sort;
        $category->parent_id = $request->parent_id;
        $category->setTranslation('content', $this->locale, $request->content);
        $category->setTranslation('description', $this->locale, '');

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');

                $media = new Media();
                $media->name = $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $path_thumbnail;
                $media->save();
                $category->setTranslation('thumbnail', $this->locale, $path_thumbnail);
            }
        }
        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $category->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $category->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $value) {
                $category->medias()->attach($value, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }
        $category->save();

        SystemSetting::updateOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->route($this->category_type.'.index')->with('success', '類別已建立');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $theater_category)
    {
        $category = $theater_category;
        // 檢查類別是否有子類別，如果有，防止刪除
        if ($category->children()->count() > 0) {
            return redirect()->back()->with('error', '無法刪除，該類別下有子類別存在。');
        }

        $category->delete();

        SystemSetting::updateOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->back()->with('success', '類別已刪除');
    }

    public function store_sort(Request $request)
    {
        return redirect()->back()->with('success', '順序已更新');
    }
}
