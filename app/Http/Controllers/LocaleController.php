<?php

namespace App\Http\Controllers;

use App\Models\Locale as ModelsLocale;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $route = 'locale';
        $locales = ModelsLocale::orderBy('status', 'desc')->get();

        return view('locale/list', compact(
            'route',
            'locales'
        ));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ModelsLocale $locale)
    {
        $ori_status = $locale->status;

        if ($ori_status == 0) {
            $locale->status = 1;
        } else {
            $locale->status = 0;
        }
        $locale->save();

        return redirect()->back()->with('success', '已更新');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
