<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MediaController extends Controller
{
    public function index(Request $request)
    {
        $mediaFiles = Media::when($request->filter, function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->filter.'%');
        })->paginate(20);

        return view('media.index', compact('mediaFiles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file',
        ]);
        $file = $request->file('media_file');
        $path_file = $file->store('media', 'public');

        $path_thumbnail = '';
        if ($request->has('media_thumbnail')) {
            $thumbnail = $request->file('media_thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');
            }
        }

        $media = new Media();
        $media->name = $file->getClientOriginalName();
        $media->file_path = $path_file;
        $media->type = $file->getMimeType();
        $media->thumbnail = $path_thumbnail;
        $media->save();

        return back()->with('success', '檔案上傳成功');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Media $medium)
    {
        $media = $medium;
        if (empty($media)) {
            abort(404);
        }

        if ($media->removeable == 1) {
            if (! empty($media->file_path) && file_exists(public_path('storage/'.$media->getRawOriginal('file_path')))) {
                // dd(public_path('storage/'.$media->getRawOriginal('file_path')));
                File::delete(public_path('storage/'.$media->getRawOriginal('file_path')));
            }
            if (! empty($media->thumbnail) && file_exists(public_path('storage/'.$media->getRawOriginal('thumbnail')))) {
                File::delete(public_path('storage/'.$media->getRawOriginal('thumbnail')));
            }

            $media->delete();

            return redirect()->back()->with('success', '內容已刪除');
        }

        return redirect()->back()->with('error', '內容不可刪除');
    }
}
