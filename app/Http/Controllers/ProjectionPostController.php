<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Media;
use App\Models\Post;
use App\Models\SystemSetting;
use Illuminate\Http\Request;

class ProjectionPostController extends Controller
{
    private $post_type = 'projection_post';

    private $locale = 'zh_cht';

    private $parent_category = false;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $route = $this->post_type;
        $posts = Post::where('type', $this->post_type)
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->orderBy('menu_order', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate(10);

        return view('posts/index', compact('route', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request, Post $projection_post)
    {
        $post = $projection_post;
        $post_type = $this->post_type;
        $route = $this->post_type;
        $locale = $this->locale;
        $categories = Category::where('type', 'projection_category')
            ->with('children')
            ->orderBy('sort', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        $post->load(['medias' => function ($query) {
            $query->where('related_locale', $this->locale);
        }, 'categories']);

        $mediaFiles = Media::when($request->filter, function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->filter.'%');
        })->paginate(20);

        return view('posts/create', compact('route', 'locale', 'post_type', 'categories', 'mediaFiles', 'post'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            // 'name' => 'required|max:255',
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $post = new Post();
        $post->name = '';
        $post->setTranslation('title', $this->locale, $request->title);
        $post->setTranslation('description', $this->locale, $request->description);
        $post->setTranslation('content', $this->locale, $request->content);
        $post->status = $request->status ? 1 : 0;
        $post->removeable = true;
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->type = $this->post_type;
        $post->save();

        if ($request->has('category')) {
            $categories_ids = [];
            foreach ($request->category as $key => $value) {
                if (! empty($value)) {
                    $categories_ids[] = $value;
                }
            }
            $post->categories()->sync($categories_ids);
        }

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');

                $media = new Media();
                $media->name = $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $path_thumbnail;
                $media->save();
                $post->setTranslation('thumbnail', $this->locale, $path_thumbnail);
                $post->save();
            }
        }

        // meida_ids[]
        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $post->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $post->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $meida_id) {
                $post->medias()->attach($meida_id, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }

        SystemSetting::updateOrCreate([
            'setting_key' => 'projection_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->route($this->post_type.'.index')->with('success', '內容已建立');
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $projection_post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Post $projection_post)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $post = $projection_post;
        $post_type = $this->post_type;
        $route = $this->post_type;
        $locale = $this->locale;

        $post->load(['medias' => function ($query) {
            $query->where('related_locale', $this->locale);
        }, 'categories']);

        foreach ($post->medias as $key => $media) {
            $media->setAttribute('url', $media->file_path);
        }

        $categories = Category::where('type', 'projection_category')->whereNull('parent_id')->orderBy('sort', 'asc')->orderBy('id', 'asc')->get();

        return view('posts/edit', compact('route', 'locale', 'post_type', 'post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $projection_post)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            // 'name' => 'required|max:255',
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $post = $projection_post;
        $post->name = '';
        $post->setTranslation('title', $this->locale, $request->title);
        $post->setTranslation('description', $this->locale, $request->description);
        $post->setTranslation('content', $this->locale, $request->content);
        $post->status = $request->status ? 1 : 0;
        $post->removeable = true;
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->type = $this->post_type;
        $post->save();

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');

                $media = new Media();
                $media->name = $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $path_thumbnail;
                $media->save();
                $post->setTranslation('thumbnail', $this->locale, $path_thumbnail);
                $post->save();
            }
        }

        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $post->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $post->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $value) {
                $post->medias()->attach($value, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }

        if ($request->has('category')) {
            $categories_ids = [];
            foreach ($request->category as $key => $value) {
                if (! empty($value)) {
                    $categories_ids[] = $value;
                }
            }
            $post->categories()->sync($categories_ids);
        }

        SystemSetting::updateOrCreate([
            'setting_key' => 'projection_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->route($this->post_type.'.index')->with('success', '內容已修改');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $projection_post)
    {
        $post = $projection_post;

        $post->medias()->sync([]);

        $post->delete();

        SystemSetting::updateOrCreate([
            'setting_key' => 'projection_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->back()->with('success', '內容已刪除');
    }
}
