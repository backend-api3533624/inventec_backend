<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Media;
use App\Models\Post;
use App\Models\Postmeta;
use App\Models\SystemSetting;
use Illuminate\Http\Request;

class TheaterPostController extends Controller
{
    private $post_type = 'theater_post';

    private $locale = 'zh_cht';

    private $post_metas = [
        'theater_video_dimension' => '',
    ];

    public function standby_video(Request $request)
    {
        $route = 'theater_standby_video.update';
        $video = SystemSetting::firstOrCreate([
            'setting_key' => 'theater_standby_video',
        ], [
            'setting_value' => '',
        ]);

        if ($request->has('file')) {
            $file = $request->file('file');
            $path_file = $file->store('media', 'public');

            $media = new Media();
            $media->name = $file->getClientOriginalName(); // 原始檔名
            $media->type = $file->getMimeType(); // uuid檔名
            $media->file_path = $path_file;
            $media->save();

            $_tmp = [
                'id' => $media->id,
                'name' => $media->name,
                'type' => $media->type,
                'file_path' => $media->getRawOriginal('file_path'),
                'updated_at' => $media->updated_at,
            ];
            $video->setting_value = json_encode($_tmp);
            $video->save();

            SystemSetting::updateOrCreate([
                'setting_key' => 'theater_last_edit_timestamp',
            ], [
                'setting_value' => date('Y-m-d H:i:s'),
            ]);

        }

        $video->media = collect(json_decode($video->setting_value, true));

        return view('system_setting.standby_video', compact('route', 'video'));
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $route = $this->post_type;
        $posts = Post::where('type', $this->post_type)
            ->when($request->filter, function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->filter.'%');
            })
            ->with('metas')
            ->orderBy('menu_order', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate(10);

        return view('posts/index', compact('route', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $post_type = $this->post_type;
        $route = $this->post_type;
        $locale = $this->locale;

        $categories = Category::where('type', 'theater_category')->orderBy('sort', 'asc')->orderBy('id', 'asc')->get();

        $post_metas = [];
        $properties = [];
        foreach ($this->post_metas as $key => $desc) {
            $_tmp = SystemSetting::where('setting_key', $key)->first();
            if (! empty($_tmp)) {
                $properties[$key] = $_tmp;
                $post_metas[$key] = $_tmp->setting_value;
            }
        }

        return view('posts/create', compact('route', 'locale', 'post_type', 'categories', 'post_metas', 'properties'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            // 'name' => 'required|max:255',
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $post = new Post();
        $post->name = '';
        $post->setTranslation('title', $this->locale, $request->title);
        $post->setTranslation('description', $this->locale, $request->description);
        $post->setTranslation('content', $this->locale, $request->content);
        $post->status = $request->status ? 1 : 0;
        $post->removeable = true;
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->type = $this->post_type;
        $post->save();

        if ($request->has('post_meta')) {
            foreach ($request->post_meta as $key => $value) {
                $post_meta = Postmeta::updateOrCreate([
                    'post_id' => $post->id,
                    'meta_key' => $key,
                ], [
                    'meta_value' => $value,
                ]);
            }
        }

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');

                $media = new Media();
                $media->name = $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $path_thumbnail;
                $media->save();
                $post->setTranslation('thumbnail', $this->locale, $path_thumbnail);
                $post->save();
            }
        }

        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $post->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $post->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $value) {
                $post->medias()->attach($value, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }

        if ($request->has('category')) {
            $categories_ids = [];
            foreach ($request->category as $key => $value) {
                if (! empty($value)) {
                    $categories_ids[] = $value;
                }
            }
            $post->categories()->sync($categories_ids);
        }

        SystemSetting::updateOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->route($this->post_type.'.index')->with('success', '內容已建立');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Post $theater_post)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $post = $theater_post;
        $post_type = $this->post_type;
        $route = $this->post_type;
        $locale = $this->locale;

        $post->load(['medias' => function ($query) {
            $query->where('related_locale', $this->locale);
        }, 'categories', 'metas']);

        $categories = Category::where('type', 'theater_category')->orderBy('sort', 'asc')->orderBy('id', 'asc')->get();

        $post_metas = [];
        $properties = [];
        foreach ($this->post_metas as $key => $desc) {
            $_tmp = SystemSetting::where('setting_key', $key)->first();
            if (! empty($_tmp)) {
                $properties[$key] = $_tmp;
                $post_metas[$key] = $_tmp->setting_value;
            }
        }

        return view('posts/edit', compact('route', 'locale', 'post_type', 'categories', 'post', 'post_metas', 'properties'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $theater_post)
    {
        if ($request->has('locale')) {
            $this->locale = $request->input('locale');
        }
        $validatedData = $request->validate([
            // 'name' => 'required|max:255',
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $post = $theater_post;
        $post->name = '';
        $post->setTranslation('title', $this->locale, $request->title);
        $post->setTranslation('description', $this->locale, $request->description);
        $post->setTranslation('content', $this->locale, $request->content);
        $post->status = $request->status ? 1 : 0;
        $post->removeable = true;
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->type = $this->post_type;
        $post->save();

        if ($request->has('post_meta')) {
            foreach ($request->post_meta as $key => $value) {
                $post_meta = Postmeta::updateOrCreate([
                    'post_id' => $post->id,
                    'meta_key' => $key,
                ], [
                    'meta_value' => $value,
                ]);
            }
        }

        if ($request->has('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            if (! empty($thumbnail)) {
                $path_thumbnail = $thumbnail->store('media', 'public');

                $media = new Media();
                $media->name = $thumbnail->getClientOriginalName();
                $media->type = $thumbnail->getMimeType();
                $media->file_path = $path_thumbnail;
                $media->save();
                $post->setTranslation('thumbnail', $this->locale, $path_thumbnail);
                $post->save();
            }
        }

        if ($request->has('media_ids')) {
            $relatedLocale = $this->locale;
            // 找出需要刪除的中介資料
            $mediaToRemove = $post->medias()->wherePivot('related_locale', $relatedLocale)->get();
            // 移除指定語系的關聯
            $post->medias()->wherePivot('related_locale', $relatedLocale)->detach($mediaToRemove->pluck('id'));
            // 然後新增指定的關聯
            foreach ($request->media_ids as $key => $value) {
                $post->medias()->attach($value, [
                    'related_locale' => $relatedLocale,
                    'item_sort' => $key,
                ]);
            }
        }

        if ($request->has('category')) {
            $categories_ids = [];
            foreach ($request->category as $key => $value) {
                if (! empty($value)) {
                    $categories_ids[] = $value;
                }
            }
            $post->categories()->sync($categories_ids);
        }

        SystemSetting::updateOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->route($this->post_type.'.index')->with('success', '內容已修改');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $theater_post)
    {
        $post = $theater_post;

        $post->medias()->sync([]);

        $post->delete();

        SystemSetting::updateOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->back()->with('success', '內容已刪除');
    }
}
