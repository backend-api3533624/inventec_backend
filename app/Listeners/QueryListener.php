<?php

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Log;

class QueryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(QueryExecuted $event)
    {
        if (env('APP_ENV') === 'local') {
            $database = $event->connection->getName();
            $username = $event->connection->getConfig()['username'];

            $sql = str_replace('?', "'%s'", $event->sql);
            $query = vsprintf($sql, $event->bindings);
            $query = sprintf("DB: %s %s %s\n%s;", $database, $username, $event->time, $query);

            Log::channel('query')->info($query);
        }
    }
}
