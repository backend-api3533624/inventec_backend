<?php

namespace App\Traits;

trait ApiResponseTrait
{
    public function apiResponse($data = null, $status = 200, $errors = '')
    {
        $response = [
            'status' => $status,
        ];

        if ($status == 200) {
            if (isset($data)) {
                $response['data'] = $data;
            }
        } else {
            $response['errors'] = $errors;
        }

        return response()->json($response, $status);
    }
}
