<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Media extends Model
{
    use HasFactory;

    protected $casts = [
        'removeable' => 'boolean',
    ];

    public function posts(): MorphToMany
    {
        return $this->morphedByMany(
            Post::class,
            'related',
            'attachments',
            'media_id',
            'related_id'
        )->withPivot('related_locale', 'item_sort');
    }

    public function categories(): MorphToMany
    {
        return $this->morphedByMany(
            Category::class,
            'related',
            'attachments',
            'media_id',
            'related_id'
        )->withPivot('related_locale', 'item_sort');
    }

    public function getFilePathAttribute($value)
    {
        return asset('storage/'.$value);
    }

    public function getThumbnailAttribute($value)
    {
        $url = asset('images/default-image.svg');
        if (! empty($value) && file_exists(public_path('storage/'.$value))) {
            $url = asset('storage/'.$value);
        }

        return $url;
    }
}
