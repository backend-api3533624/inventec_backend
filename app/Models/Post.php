<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Translatable\HasTranslations;

class Post extends Model
{
    use HasFactory;
    use HasTranslations;

    protected $fillable = [
        'name',
        'title',
        'description',
        'content',
        'thumbnail',
        'status',
        'removeable',
        'type',
        'menu_order',
        'post_parent',
    ];

    public $translatable = ['name', 'title', 'content', 'description', 'thumbnail'];

    public function parent()
    {
        return $this->belongsTo(Post::class, 'post_parent')->orderBy('menu_order', 'ASC')->orderBy('id', 'ASC');
    }

    public function children()
    {
        return $this->hasMany(Post::class, 'post_parent')->orderBy('menu_order', 'ASC')->orderBy('id', 'ASC');
    }

    public function metas()
    {
        return $this->hasMany(Postmeta::class, 'post_id')->orderBy('id', 'ASC');
    }

    public function medias(): MorphToMany
    {
        return $this->morphToMany(
            Media::class,
            'related',
            'attachments',
            'related_id',
            'media_id'
        )->withPivot('related_locale', 'item_sort')->orderBy('item_sort');
    }

    public function categories(): MorphToMany
    {
        return $this->morphToMany(
            Category::class,
            'related',
            'category_relationships',
            'related_id',
            'category_id'
        )->withPivot('item_sort')->orderBy('item_sort');
    }
}
