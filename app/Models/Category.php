<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory;
    use HasTranslations;

    protected $fillable = [
        'name',
        'status',
        'removeable',
        'type',
        'content',
        'description',
        'thumbnail',
        'sort',
        'parent_id',
    ];

    public $translatable = ['name', 'content', 'description', 'thumbnail'];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id')->orderBy('sort', 'ASC')->orderBy('id', 'ASC');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->orderBy('sort', 'ASC')->orderBy('id', 'ASC');
    }

    public function posts(): MorphToMany
    {
        return $this->morphedByMany(
            Post::class,
            'related',
            'category_relationships',
            'category_id',
            'related_id'
        )->withPivot('item_sort')->orderBy('item_sort');
    }

    public function medias(): MorphToMany
    {
        return $this->morphToMany(
            Media::class,
            'related',
            'attachments',
            'related_id',
            'media_id'
        )->withPivot('related_locale', 'item_sort')->orderBy('item_sort');
    }
}
