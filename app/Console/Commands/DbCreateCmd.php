<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use mysqli;

class DbCreateCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create database according to .env setting';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! $root_password = env('DB_ROOT_PASSWORD')) {
            $this->error('DB_ROOT_PASSWORD password not set in .env');

            return 0;
        }
        if (! $database = env('DB_DATABASE')) {
            $this->error('DB_DATABASE password not set in .env');

            return 0;
        }
        if (! $username = env('DB_USERNAME')) {
            $this->error('DB_USERNAME password not set in .env');

            return 0;
        }
        if (! $password = env('DB_PASSWORD')) {
            $this->error('DB_PASSWORD password not set in .env');

            return 0;
        }
        if (! $host = env('DB_HOST')) {
            $this->error('DB_HOST password not set in .env');

            return 0;
        }

        $mysqli = new mysqli($host, 'root', $root_password);

        $mysqli->query("CREATE USER '$username'@'%' IDENTIFIED BY '$password';");
        $mysqli->query("GRANT USAGE ON *.* TO '$username'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;");
        $mysqli->query("CREATE DATABASE IF NOT EXISTS `$database` DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;");
        $mysqli->query("GRANT ALL PRIVILEGES ON `$database`.* TO '$username'@'%';");

        $this->info('Create database and user successfully.');

        return 0;
    }
}
