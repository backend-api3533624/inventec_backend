<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->ask('What is your name?');
        $email = $this->ask('What is your email ?');
        if (User::where('email', $email)->first()) {
            $this->error('Email is already exist!');

            return;
        }
        while (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = $this->ask('Illegal email, please enter again');
        }
        $password = $this->secret('What is your password?');
        while (! preg_match('/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z\W]{8,20}$/', $password)) {
            $password = $this->secret('Password format is incorrect. 8-20 characters, including at least one number and one letter.');
        }
        $password_confirmed = $this->secret('Enter your password again:');
        if ($password != $password_confirmed) {
            $this->error('Password and confirm password does not match!');

            return;
        }
        try {
            $now = date('Y-m-d H:i:s');
            $admin = new User([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $admin->save();
        } catch (\Exception $err) {
            $this->error($err->getMessage());

            return;
        }
        $this->info('Created admin success!');

    }
}
