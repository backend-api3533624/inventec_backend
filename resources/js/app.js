import "./bootstrap";
import bsCustomFileInput from "bs-custom-file-input";
import Sortable from "sortablejs";

import "./vue/postEditApp";
import "./vue/categoryEditApp";

import * as Vue from "vue";

window.Vue = Vue;
