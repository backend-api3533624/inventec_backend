import { createApp, ref, onMounted } from "vue";
import axios from "axios";

const postEditApp = document.getElementById("postEditApp");

const getIdForVue = document.getElementById("getIdForVue").textContent;

//特定ＩＤ的post頁面底下的Media
const getPostMedias = document.getElementById("getPostMediasJson");
const getPostMediasJson = getPostMedias?.getAttribute("data-json");
let postMedias = null;
if (getPostMediasJson !== undefined) {
    postMedias = JSON.parse(getPostMediasJson);
}

if (postEditApp && getIdForVue) {
    createApp({
        name: "PostEditApp",
        setup() {
            const videoMedia = ref([]);
            const videoPagination = ref([]);
            const pictureMedia = ref([]);
            const picturePagination = ref([]);
            const selectedVideo = ref([]);
            const selectedPicture = ref([]);
            const newSelectedVideo = ref([]);
            const newSelectedPicture = ref([]);
            const postId = ref(getIdForVue);

            console.log(postMedias);
            if (postMedias) {
                for (const media of postMedias) {
                    if (media.type === "video/mp4") {
                        selectedVideo.value.push(media);
                        newSelectedVideo.value.push(media);
                    } else if (media.type === "image/jpeg") {
                        selectedPicture.value.push(media);
                        newSelectedPicture.value.push(media);
                    }
                }
                console.log("selectedVideo.value", selectedVideo.value);
                console.log("selectedPicture.value", selectedPicture.value);
            }

            const fetchMedia = (filterType, mediaValue, paginationValue) => {
                axios
                    .get(
                        `/api/media?filter=${filterType}&post_id=${postId.value}`
                    )
                    .then((response) => {
                        mediaValue.value = response.data.data;
                        paginationValue.value = {
                            currentPage: response.data.meta.current_page,
                            lastPage: response.data.meta.last_page,
                        };
                    })
                    .catch((error) => {
                        console.error("Error fetching media:", error);
                    });
            };

            const handleOpenVideoMedia = () => {
                fetchMedia("mp4", videoMedia, videoPagination);
            };

            const handleOpenPictureMedia = () => {
                fetchMedia("jpg", pictureMedia, picturePagination);
            };

            const changePage = (number, type) => {
                axios
                    .get(
                        `/api/media?filter=${type}&page=${number}&post_id=${postId.value}`
                    )
                    .then((response) => {
                        if (type === "jpg") {
                            pictureMedia.value = response.data.data;
                            picturePagination.value = {
                                currentPage: response.data.meta.current_page,
                                lastPage: response.data.meta.last_page,
                            };
                        } else if (type === "mp4") {
                            videoMedia.value = response.data.data;
                            videoPagination.value = {
                                currentPage: response.data.meta.current_page,
                                lastPage: response.data.meta.last_page,
                            };
                        }
                    })
                    .catch((error) => {
                        console.error("Error fetching media:", error);
                    });
            };

            //刪除已經選擇的圖片或影片
            const handleDelete = (event, id) => {
                event.preventDefault();

                //刪除傳給後端的mediaIDs
                const formGroupElements = document.querySelector(
                    `.form-group[data-media-id="${id}"]`
                );
                formGroupElements.remove();

                //更新已選內容的值
                selectedVideo.value = selectedVideo.value.filter(
                    (video) => video.id !== id
                );
                selectedPicture.value = selectedPicture.value.filter(
                    (picture) => picture.id !== id
                );
                newSelectedVideo.value = [...selectedVideo.value];
                newSelectedPicture.value = [...selectedPicture.value];
            };

            const handleSave = (type) => {
                const selectedMedia =
                    type === "mp4"
                        ? newSelectedVideo.value
                        : newSelectedPicture.value;
                const mediaIdSelector =
                    type === "mp4" ? ".videoMediaIds" : ".pictureMediaIds";

                const mediaId = document.querySelector(mediaIdSelector);

                let mediaHtml = "";

                selectedMedia.forEach(function (media) {
                    mediaHtml +=
                        '<div class="form-group col-xl-6 col-12" data-media-id="' +
                        media.id +
                        '">';
                    mediaHtml +=
                        '    <input class="form-control" type="text" name="media_ids[]" value="' +
                        media.id +
                        '" style="display:none">';
                    mediaHtml += "</div>";
                });

                if (type === "mp4") {
                    selectedVideo.value = [...newSelectedVideo.value];
                } else if (type === "jpg") {
                    selectedPicture.value = [...newSelectedPicture.value];
                }
                mediaId.innerHTML = mediaHtml;
            };

            const handleCancel = () => {
                newSelectedVideo.value = [...selectedVideo.value];
                newSelectedPicture.value = [...selectedPicture.value];
            };

            const handleCheckBoxChange = (e, video, type) => {
                const videoId = video.id;

                if (type === "mp4") {
                    const index = newSelectedVideo.value.findIndex(
                        (item) => item.id === videoId
                    );
                    if (index === -1) {
                        newSelectedVideo.value.push(video);
                    } else {
                        newSelectedVideo.value.splice(index, 1);
                    }
                } else if (type === "jpg") {
                    const index = newSelectedPicture.value.findIndex(
                        (item) => item.id === videoId
                    );
                    if (index === -1) {
                        newSelectedPicture.value.push(video);
                    } else {
                        newSelectedPicture.value.splice(index, 1);
                    }
                }
            };

            const handleCheckRadioChange = (e, video, type) => {
                if (type === "mp4") {
                    newSelectedVideo.value = [];
                    newSelectedVideo.value.push(video);
                } else if (type === "jpg") {
                    newSelectedPicture.value = [];
                    newSelectedPicture.value.push(video);
                }
            };

            const isVideoSelected = (videoId, type) => {
                const selectedMedia =
                    type === "mp4"
                        ? newSelectedVideo.value
                        : newSelectedPicture.value;
                return selectedMedia.some((video) => video.id === videoId);
            };

            onMounted(() => {
                console.log("Component mounted");
                fetchMedia("mp4", videoMedia, videoPagination);
                fetchMedia("jpg", pictureMedia, picturePagination);
            });
            return {
                handleOpenVideoMedia,
                handleOpenPictureMedia,
                videoMedia,
                pictureMedia,
                picturePagination,
                videoPagination,
                changePage,
                handleSave,
                handleCheckBoxChange,
                isVideoSelected,
                handleCheckRadioChange,
                handleDelete,
                selectedVideo,
                selectedPicture,
                handleCancel,
            };
        },
    }).mount("#postEditApp");
}
