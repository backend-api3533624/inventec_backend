@extends('adminlte::page')

@section('title', 'Dashboard')

@section('meta_tags')
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
@endsection

@section('content_header')
    <h1>Welcome!</h1>
@stop

@section('content')

@endsection

@section('css')
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
