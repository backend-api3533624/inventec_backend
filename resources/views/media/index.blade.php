@extends('layouts.app')

@section('content_header')
    <h1>素材管理</h1>
@stop

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            @include('media.upload')
            <div class="row">
                <div class="col">
                    <div class="pagination pagination-sm">
                        <!-- pagination  -->
                        {{ $mediaFiles->appends(request()->input())->links() }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">檔案清單
                            <div class="card-tools">
                                <form action="{{ route('media.index') }}" method="GET">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="filter" class="form-control float-right"
                                            placeholder="Search" value="{{ request()->input('filter') }}">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-light">
                                <thead>
                                    <tr>
                                        <th style="width: 80px">編號</th>
                                        <th style="width: 200px">預覽縮圖</th>
                                        <th>名稱</th>
                                        <th style="width: 100px">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($mediaFiles as $file)
                                        <tr>
                                            <th>{{ $file->id }}</th>
                                            <td>
                                                @if ($file->type === 'image/jpeg')
                                                    <img width="192" height="108" src="{{ $file->file_path }}"
                                                        alt="" class="thumbnail" style="object-fit: cover">
                                                @elseif (!empty($file->thumbnail))
                                                    <img width="192" height="108" src="{{ $file->thumbnail }}"
                                                        alt="" class="thumbnail" style="object-fit: cover">
                                                @else
                                                    <img width="192" height="108"
                                                        src="{{ asset('images/default-image.svg') }}" alt=""
                                                        class="thumbnail" style="object-fit: cover">
                                                @endif
                                            </td>
                                            <td>{{ $file->name }}</td>
                                            <td>
                                                @if ($file->removeable == 1)
                                                    @if (in_array(
                                                            'admin',
                                                            Auth::user()->getRoleNames()->toArray()))
                                                        <form action="{{ route('media.destroy', $file) }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger" type="submit"
                                                                onclick="return confirm('確定要刪除這個類別 \#{{ $file->id }}嗎？');">刪除</button>
                                                        </form>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="col">
                                <div class="pagination pagination-sm">
                                    <!-- pagination  -->
                                    {{ $mediaFiles->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
