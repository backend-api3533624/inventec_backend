<div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">上傳媒體</div>
            <div class="card-body">
                <div id="alertContainer" class="mt-3"></div>
                <div>
                    <label for="upload_media_file">檔案</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="upload_media_file" name="media_file"
                                accept="image/jpeg, video/mp4">
                            <label class="custom-file-label" for="upload_media_file">Choose file</label>
                        </div>
                    </div>
                </div>
                <div>
                    <label for="upload_thumbnail">縮圖</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="upload_thumbnail" name="media_thumbnail"
                                accept="image/jpeg">
                            <label class="custom-file-label" for="upload_thumbnail">Choose file</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button id="submitBtn" class="btn btn-primary">Upload</button>
            </div>
        </div>
    </div>
</div>

@section('plugins.BsCustomFileInput', true)

@section('js')
    @parent
    <script>
        $(document).ready(function() {
            bsCustomFileInput.init();

            $("#submitBtn").on('click', function(event) {
                event.preventDefault();

                $("#submitBtn").prop("disabled", true);

                let formData = new FormData();

                formData.append('media_file', $('#upload_media_file')[0].files[0]);
                formData.append('media_thumbnail', $('#upload_thumbnail')[0].files[0]);
                formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

                $.ajax({
                    url: "/api/media",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    xhrFields: {
                        withCredentials: true // Include cookies for CSRF (SPA)
                    },
                    success: function(response) {
                        console.log(response); // Handle the response from the server
                        let alert = $(
                            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                            '上傳成功' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '</div>');
                        $('#alertContainer').append(alert);
                        $('#upload_media_file').val(null);
                        $('#upload_media_file').next('.custom-file-label').html('Choose file');

                        $('#upload_thumbnail').val(null);
                        $('#upload_thumbnail').next('.custom-file-label').html('Choose file');

                        setTimeout(function() {
                            alert.alert('close');
                        }, 1000);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        let alert = $(
                            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                            '上傳失敗' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '</div>');
                        $('#alertContainer').append(alert);
                        setTimeout(function() {
                            alert.alert('close');
                        }, 5000);
                        console.log(textStatus, errorThrown); // Handle any errors
                    },
                    complete: function() {
                        // Enable the upload button after the request is complete
                        $("#submitBtn").prop("disabled", false);
                    }
                });
            });
        });
    </script>
@endsection
