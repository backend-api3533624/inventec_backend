@extends('adminlte::page')

@section('content_header')
    <h1>語系管理</h1>
@stop

@section('content')
    <section class="content" style="max-width: 800px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-body">
                            <ul class="todo-list ui-sortable" data-widget="todo-list">
                                @foreach ($locales as $locale)
                                    <li>
                                        <div class="row d-flex align-items-center" style="min-height: 60px;">
                                            <div class="d-flex justify-content-center" style="width: 60px">
                                                @if ($locale->status)
                                                    <div
                                                        style="background-color: green; width: 18px; height: 18px; border-radius: 50%;">
                                                    </div>
                                                @else
                                                    <div
                                                        style="background-color: rgb(190, 190, 190); width: 18px; height: 18px; border-radius: 50%;">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col">
                                                <strong>{{ $locale->chinese }}</strong>
                                            </div>
                                            <div style="width: 160px">
                                                <form action="{{ route($route . '.destroy', $locale) }}" method="POST">
                                                    @csrf
                                                    @method('PATCH')
                                                    @if ($locale->status)
                                                        <button class="btn btn-danger" type="submit"
                                                            onclick="return confirm('確定要更新 \#{{ $locale->id }}嗎？');">
                                                            下架語系</button>
                                                    @else
                                                        <button class="btn btn-primary" type="submit"
                                                            onclick="return confirm('確定要更新 \#{{ $locale->id }}嗎？');">
                                                            上架語系</button>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection
