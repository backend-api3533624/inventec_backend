@extends('adminlte::page')

@section('content_header')
    <h1>使用者清單</h1>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('user.create') }}" class="btn btn-success">建立使用者</a>
                    <div class="card card-primary">
                        <div class="card-header">使用者清單
                            <div class="card-tools">
                                <form action="{{ route('user.index') }}" method="GET">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="filter" class="form-control float-right"
                                            placeholder="Search" value="{{ request()->input('filter') }}">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                @foreach ($users as $user)
                                    <li>
                                        <a href="{{ route('user.edit', $user->id) }}">{{ $user->name }}
                                            {{ $user->email }}</a>
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                        <div class="card-footer">
                            <div class="col">
                                <div class="pagination pagination-sm">
                                    <!-- pagination  -->
                                    {{ $users->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('plugins.BsCustomFileInput', true)

@section('js')
    <script>
        $(document).ready(function() {
            bsCustomFileInput.init()
        })
    </script>
@endsection
