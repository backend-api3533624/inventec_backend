<div class="form-group col-xl-6 col-12">
    <label for="title">類別名稱*</label>
    <input id="title" class="form-control" type="text" name="content" required
        value="{{ !empty($category) ? $category->getTranslation('content', $locale, 0) : '' }}">
    <small id="emailHelp" class="form-text text-muted">多語系</small>
</div>
