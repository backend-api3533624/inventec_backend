<div class="form-group col-xl-6 col-12">
    <label for="upload_file">封面圖片</label>
    <div class="input-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="upload_file" name="thumbnail" accept="image/jpeg">
            <label class="custom-file-label" for="upload_file">選擇檔案</label>
        </div>
    </div>
</div>
<div class="row mt-3 col-xl-6 col-12 d-flex">
    <div class="mr-2" style="width: 192px; height: 108px;">
        <!-- 圖片預覽 -->
        <img id="preview_image"
            src="{{ !empty($category) && !empty($category->getTranslation('thumbnail', $locale, 0)) ? asset('storage/' . $category->getTranslation('thumbnail', $locale, 0)) : asset('images/default-image.svg') }}"
            alt="Preview" style="width: 192px; height: 108px; object-fit:cover;">
    </div>
    <div class="d-flex flex-column justify-content-center ml-2" style="width: 180px; height: 108px;">
        <h6 class="text-secondary small">上傳檔案限制</h6>
        <span class="text-secondary small">圖片格式：jpg<br>尺寸：建議 1920*1080<br>檔案大小：5MB</span>
    </div>
</div>
