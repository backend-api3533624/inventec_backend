<div class="form-group mt-3 col-xl-6 col-12" style="display: none">
    <label for="category_sort">類別排序*</label>
    <input id="category_sort" class="form-control" type="text" name="sort"
        value="{{ !empty($category) ? $category->sort : 0 }}">
</div>
