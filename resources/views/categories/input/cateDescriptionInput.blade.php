<div class="form-group col-xl-6 col-12">
    <label for="description">介紹*</label>
    <textarea id="description" class="form-control" type="text" name="description" required
        value="{{ !empty($category) ? $category->getTranslation('description', $locale, 0) : '' }}"></textarea>
    <small id="emailHelp" class="form-text text-muted">多語系</small>
</div>
