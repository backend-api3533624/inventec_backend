<div class="form-group col-12">
    <label for="status">上下架狀態</label>
    <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitch1"
            {{ !empty($category) && $category->status ? 'checked' : '' }} name="status"
            {{ in_array('admin',Auth::user()->getRoleNames()->toArray())? '': 'disabled' }}>
        <label class="custom-control-label" for="customSwitch1"></label>
    </div>
</div>
