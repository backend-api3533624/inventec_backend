@extends('adminlte::page')

@section('meta_tags')
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
@endsection

@section('content_header')
    @php
        $routeLabels = [
            'tvwall_category' => '電視牆／建立分類',
            'theater_category' => '動態劇院／建立分類',
            'desktop_category' => '互動桌／建立分類',
            'cockpit_category' => '智慧座艙／建立分類',
            'projection_category' => '光雕投影牆／建立分類',
            'fans3d_category' => '3D風扇／建立分類',
        ];
        $defaultLabel = '建立分類';
        $route = $category_type;
    @endphp

    <h1>
        {{ $routeLabels[$category_type] ?? $defaultLabel }}
    </h1>
@stop

@section('content')
    <div id="categoryEditApp">
        @include('module.alerts')
        <div class="container-fluid">
            <div id="getIdForVue" style="display:none">0</div>
            <form method="POST" action="{{ route($category_type . '.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="locale" value="{{ $locale }}">
                <input type="hidden" name="category_type" value="{{ $category_type }}">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="form-group col-xl-6 col-12">
                                    <label for="locale">語系*</label>
                                    <select id="locale" class="form-control" name="locale">
                                        @php
                                            $app_locales = App\Models\Locale::where('status', 1)->get();
                                        @endphp
                                        @foreach ($app_locales as $app_locale)
                                            <option value="{{ $app_locale->code }}">{{ $app_locale->chinese }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- 上層類別 --}}
                                @if (isset($parent_category) && $parent_category == true)
                                    <div class="form-group col-xl-6 col-12">
                                        <label for="inputStatus">上層類別*</label>
                                        <select id="inputStatus" name="parent_id" class="form-control custom-select">
                                            @foreach (App\Models\Category::all() as $parent)
                                                @php
                                                    if ($parent->type !== 'projection_category' || $parent->removeable !== 0) {
                                                        continue;
                                                    }
                                                @endphp
                                                <option value="{{ $parent->id }}">
                                                    {{ $parent->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                {{-- 類別名稱 --}}
                                @include('categories.input.cateTitleInput')
                                {{-- 類別介紹 互動桌  --}}
                                @if ($category_type === 'desktop_category')
                                    @include('categories.input.cateDescriptionInput')
                                @endif
                                {{-- 上下架 --}}
                                @include('categories.input.cateStatusInput')
                                {{-- 封面圖片上傳及預覽 --}}
                                @if ($category_type !== 'projection_category')
                                    @include('categories.input.cateThumbnailInput')
                                @endif
                                {{-- 類別排序 --}}
                                @include('categories.input.cateSortInput')
                                {{-- 互動桌 上傳三個影片 --}}
                                @if ($category_type === 'desktop_category')
                                    <div class="form-group col-xl-6 col-12 mt-3">
                                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                            data-target="#mediaUploadVideo" @click="handleOpenVideoMedia">瀏覽影片媒體</button>
                                    </div>
                                    <!-- 使用者看到的已經選擇的影片媒體 -->
                                    @include('posts.input.attachments', ['mediaType' => 'video'])
                                @endif
                                <!-- 儲存時送到後端的mediaID -->
                                @include('posts.input.mediaInput')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 mb-3">
                        <a href="{{ route($category_type . '.index') }}" class="btn btn-secondary mr-3">取消</a>
                        <input type="submit" value="建立類別" class="btn btn-success float-right">
                    </div>
                </div>
            </form>
        </div>
        <!-- 選擇媒體的modal -->
        @include('posts.input.medioUpload', ['mediaType' => 'mp4', 'modalId' => 'mediaUploadVideo'])
    </div>
@endsection

@section('plugins.BsCustomFileInput', true)

@include('categories.script.editscript')
