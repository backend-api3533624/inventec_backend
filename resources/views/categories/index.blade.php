@extends('adminlte::page')

@section('content_header')
    @php
        $routeLabels = [
            'tvwall_category' => '電視牆／類別',
            'theater_category' => '動態劇院／類別',
            'desktop_category' => '互動桌／類別',
            'cockpit_category' => '智慧座艙／類別',
            'projection_category' => '光雕投影牆／類別',
            'fans3d_category' => '3D風扇／類別',
        ];
        $defaultLabel = '類別';
    @endphp

    <h1>
        {{ $routeLabels[$route] ?? $defaultLabel }}
    </h1>
@stop

@php
    $categories_removeable = $categories->where('removeable', 1);
    $categories_fixed = $categories->where('removeable', 0);
@endphp

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-3 right">
                <div class="mr-3">
                    <a href="{{ route($route . '.create') }}" class="btn btn-success"><i class="fa fa-solid fa-plus"></i>
                        新增類別</a>
                </div>
                {{-- 排序按鈕 --}}
                @if (in_array('admin', Auth::user()->getRoleNames()->toArray()))
                    @include('components.sortButton')
                @endif
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        @if (in_array($route, [
                                'tvwall_category',
                                'theater_category',
                                'cockpit_category',
                                'desktop_category',
                                'fans3d_category',
                            ]))
                            <div class="card-header">類別清單
                                @if (0)
                                    <div class="card-tools">
                                        <form action="{{ route($route . '.index') }}" method="GET">
                                            <div class="input-group input-group-sm" style="width: 150px;">
                                                <input type="text" name="filter" class="form-control float-right"
                                                    placeholder="Search" value="{{ request()->input('filter') }}">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-default">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                            <div class="card-body">
                                <ul class="todo-list ui-sortable" data-widget="todo-list">
                                    @foreach ($categories as $category)
                                        @php
                                            if ($category->removeable == 0) {
                                                continue;
                                            }
                                            $media = $category->medias()->first();
                                        @endphp
                                        <li>
                                            <div class="row d-flex align-items-center" style="min-height: 100px;">
                                                <div style="width: 60px">
                                                    <span class="handle ui-sortable-handle">
                                                    </span>
                                                    <span class="category_id"
                                                        style="display:none">{{ $category->id }}</span>
                                                </div>
                                                <div style="width: 192px; height: 108px">
                                                    <img class="img-fluid"
                                                        style="width: 192px; height: 108px; object-fit: cover;"
                                                        src="{{ !empty($category) && !empty($category->getTranslation('thumbnail', 'zh_cht')) ? asset('storage/' . $category->getTranslation('thumbnail', 'zh_cht')) : asset('images/default-image.svg') }}"
                                                        alt="thumbnail">
                                                </div>
                                                <div class="d-flex justify-content-center" style="width: 80px">
                                                    @if ($category->status)
                                                        上架
                                                    @else
                                                        下架
                                                    @endif
                                                </div>
                                                <div class="col">
                                                    <a
                                                        href="{{ route($category->type . '.edit', $category->id) }}">{{ $category->getTranslation('content', 'zh_cht') }}</a>
                                                </div>
                                                <div style="width: 160px">
                                                    @if (in_array('admin', Auth::user()->getRoleNames()->toArray()))
                                                        <form action="{{ route($category->type . '.destroy', $category) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="{{ route($category->type . '.edit', $category->id) }}"
                                                                class="btn btn-info btn-sm btn-edit">
                                                                <i class="fa fas fa-pencil-alt btn-edit"></i> 編輯
                                                            </a>
                                                            @if ($category->removeable == 1)
                                                                <button class="btn btn-danger btn-sm btn-delete"
                                                                    type="submit"
                                                                    onclick="return confirm('確定要刪除這個類別 \#{{ $category->id }}嗎？');">
                                                                    <i class="fas fa-trash"></i> 刪除
                                                                </button>
                                                            @endif
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                        @if ($category->children->isNotEmpty())
                                            @each('categories.partials.category', $category->children, 'category')
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-footer">
                            </div>
                        @endif
                    </div>
                </div>
                {{-- 光雕投影牆－類別清單 或 電視牆－各國主題 --}}
                @if (in_array($route, ['tvwall_category', 'projection_category']))
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">{{ $route === 'tvwall_category' ? '各國主題' : '類別清單' }}</div>
                            <div class="card-body">
                                <ul class="todo-list">
                                    @foreach ($categories as $category)
                                        @php
                                            if ($category->removeable == 1) {
                                                continue;
                                            }
                                            $media = $category->medias()->first();
                                        @endphp
                                        <li style="border-bottom: 2px solid #FFFFFF; margin-bottom: 0">
                                            <div class="row d-flex align-items-center" style="min-height: 100px;">
                                                <div style="width: 60px">
                                                    <span class="category_id"
                                                        style="display:none">{{ $category->id }}</span>
                                                </div>
                                                <div style="width: 192px; height: 108px">
                                                    @if ($route === 'projection_category')
                                                        <img class="img-fluid"
                                                            style="width: 192px; height: 108px; object-fit: cover;"
                                                            src="{{ !empty($category) && !empty($category->thumbnail) ? asset($category->thumbnail) : asset('images/default-image.svg') }}"
                                                            alt="thumbnail">
                                                    @else
                                                        <img class="img-fluid"
                                                            style="width: 192px; height: 108px; object-fit: cover;"
                                                            src="{{ !empty($category) && !empty($category->getTranslation('thumbnail', 'zh_cht')) ? asset('storage/' . $category->getTranslation('thumbnail', 'zh_cht')) : asset('images/default-image.svg') }}"
                                                            alt="thumbnail">
                                                    @endif
                                                </div>
                                                <div class="d-flex justify-content-center" style="width: 80px">
                                                    @if ($category->status)
                                                        上架
                                                    @else
                                                        下架
                                                    @endif
                                                </div>
                                                <div class="col">
                                                    @if ($route === 'projection_category')
                                                        <div>{{ $category->getTranslation('content', 'zh_cht', 0) }}</div>
                                                    @else
                                                        <a
                                                            href="{{ route($category->type . '.edit', $category->id) }}">{{ $category->getTranslation('content', 'zh_cht') }}</a>
                                                    @endif
                                                </div>
                                                @if ($category->parent_id > 0)
                                                    <div class="col-sm-1">
                                                        {{ $category->parent_id }}
                                                    </div>
                                                @endif
                                                <div style="width: 160px">
                                                    @if (in_array('admin', Auth::user()->getRoleNames()->toArray()))
                                                        <form action="{{ route($category->type . '.destroy', $category) }}"
                                                            method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            @if (in_array($route, ['tvwall_category']))
                                                                <a href="{{ route($category->type . '.edit', $category->id) }}"
                                                                    class="btn btn-info btn-sm btn-edit"><i
                                                                        class="fa fas fa-pencil-alt btn-edit">
                                                                    </i> 編輯</a>
                                                            @endif

                                                            @if ($category->removeable == 1)
                                                                <button class="btn btn-danger btn-sm btn-delete"
                                                                    type="submit"
                                                                    onclick="return confirm('確定要刪除這個類別 \#{{ $category->id }}嗎？');"><i
                                                                        class="fas fa-trash">
                                                                    </i> 刪除</button>
                                                            @endif
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                        @if ($category->children->isNotEmpty())
                                            <div class="todo-list ui-sortable">
                                                @each('categories.partials.category', $category->children, 'category')
                                            </div>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            let route = "{{ $route }}"

            // 開啟排序功能
            $('#start_sort_function').click(function() {
                event.preventDefault();

                $('.btn-edit, .btn-delete').prop('disabled', true);
                $('.btn-edit, .btn-delete').addClass('disabled').click(function(e) {
                    e.preventDefault();
                });

                $('.ui-sortable-handle').append('<i class="fas fa-ellipsis-v"></i>');
                $('.ui-sortable-handle').append('<i class="fas fa-ellipsis-v"></i>');
                $(this).hide();
                $('#store_sort').show();
            });
            // 儲存排序
            $("#store_sort").click(function() {
                event.preventDefault();

                $('.btn-edit, .btn-delete').prop('disabled', false);
                $('.btn-edit, .btn-delete').removeClass('disabled').off('click');

                let numbers = [];
                $('.todo-list.ui-sortable .category_id').each(function() {
                    let number = $(this).text().trim();
                    numbers.push(number);
                });
                $('.ui-sortable-handle').find('i.fa-ellipsis-v').remove();
                $(this).hide();
                $('#start_sort_function').show();

                let formData = new FormData();
                formData.append('type', route);
                formData.append('cate_sort', numbers.join(','));

                $.ajax({
                    url: "/api/category/sort", // Change to your server script
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    xhrFields: {
                        withCredentials: true // Include cookies for CSRF (SPA)
                    },
                    success: function(response) {
                        console.log(response); // Handle the response from the server
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown); // Handle any errors
                    }
                });
            });

            // Enable sortable feature
            $(".todo-list.ui-sortable").sortable({
                placeholder: "ui-state-highlight",
                handle: ".handle",
                update: function(event, ui) {
                    // Handle the new order
                    console.log("New order:", $(this).sortable("toArray"));
                }
            });
            $(".todo-list").disableSelection();
        })
    </script>
@endsection
