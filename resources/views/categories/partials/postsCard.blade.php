<ul class="todo-list ui-sortable" data-widget="todo-list">
    @if (!empty($posts) && count($posts) > 0)
        @foreach ($posts as $post)
            @php
                $thumbnail = 'images/default-image.svg';
                if (!empty($post->getTranslation('thumbnail', $locale, 0))) {
                    $thumbnail = $post->getTranslation('thumbnail', $locale, 0);
                }
            @endphp
            <li>
                <div class="row d-flex align-items-center" style="min-height: 100px;">
                    <div style="width: 60px">
                        <span class="handle ui-sortable-handle-post">
                        </span>
                        <span class="post_id" style="display:none">{{ $post->id }}</span>
                    </div>
                    <div style="width: 192px; height: 108px; padding:0">
                        <img style="width: 192px; height: 108px; object-fit: cover;"
                            src="{{ asset($thumbnail !== 'images/default-image.svg' ? 'storage/' . $thumbnail : 'images/default-image.svg') }}"
                            alt="thumbnail">
                    </div>
                    <div style="width: 80px">
                        @if ($post->status)
                            上架
                        @else
                            下架
                        @endif
                    </div>
                    <div class="col">
                        <a
                            href="{{ route($post->type . '.edit', $post->id) }}">{{ $post->getTranslation('title', $locale, 0) }}</a>
                    </div>
                </div>
            </li>
        @endforeach
    @else
        <div class="d-flex bg-light w-100 mt-3 flex-column align-items-center justify-content-center"
            style="height:100px">
            <h5>此分類尚無任何內容</h5>
        </div>
    @endif
</ul>
