@php
    $media = $category->medias()->first();
@endphp
<li style="border-bottom: 2px solid #FFFFFF; margin-bottom: 0">
    <div class="row">
        <div style="width: 60px">
            <span class="handle ui-sortable-handle">

            </span>
            <span class="category_id" style="display:none">{{ $category->id }}</span>
        </div>
        <div style="width:192px">
            @if (!empty($media))
                <img class="img-thumbnail" src="{{ $media->thumbnail }}" alt="">
            @endif
        </div>
        <div style="width: 80px" class="d-flex justify-content-center">
            @if ($category->status)
                上架
            @else
                下架
            @endif
        </div>
        <div class="col">
            <a
                href="{{ route($category->type . '.edit', $category->id) }}">{{ $category->getTranslation('content', 'zh_cht', 0) }}</a>
        </div>
        @if ($category->parent_id > 0)
            <div class="col-sm-1" style="display:none">
                {{ $category->parent_id }}
            </div>
        @endif
        <div class="col-sm-2">
            @if (in_array(
                    'admin',
                    Auth::user()->getRoleNames()->toArray()))
                <form action="{{ route($category->type . '.destroy', $category) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="{{ route($category->type . '.edit', $category->id) }}" class="btn btn-info btn-sm"><i
                            class="fa fas fa-pencil-alt">
                        </i> 編輯</a>
                    @if ($category->removeable == 1)
                        <button class="btn btn-danger btn-sm" type="submit"
                            onclick="return confirm('確定要刪除這個類別 \#{{ $category->id }}嗎？');"><i class="fas fa-trash">
                            </i> 刪除</button>
                    @endif
                </form>
            @endif
        </div>
    </div>
</li>
