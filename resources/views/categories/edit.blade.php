@extends('adminlte::page')

@section('meta_tags')
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
@endsection

@section('content_header')
    @php
        $routeLabels = [
            'tvwall_category' => '電視牆／編輯分類',
            'theater_category' => '動態劇院／編輯分類',
            'desktop_category' => '互動桌／編輯分類',
            'cockpit_category' => '智慧座艙／編輯分類',
            'projection_category' => '光雕投影牆／編輯分類',
            'fans3d_category' => '3D風扇／編輯分類',
        ];
        $defaultLabel = '編輯分類';
        $route = $category_type;
    @endphp

    <h1>
        {{ $routeLabels[$category_type] ?? $defaultLabel }}
    </h1>
@stop

@section('content')
    <div id="categoryEditApp">
        @include('module.alerts')
        <div class="container-fluid">
            <div id="getIdForVue" style="display:none">{{ $category->id }}</div>
            <div class="mb-3">
                @php
                    $app_locales = App\Models\Locale::where('status', 1)->get();
                @endphp
                @foreach ($app_locales as $app_locale)
                    <a role="button"
                        href="{{ route($category->type . '.edit', [$category->type => $category->id, 'locale' => $app_locale->code]) }}"
                        class="btn {{ $locale == $app_locale->code ? 'btn-primary' : '' }}">{{ $app_locale->chinese }}</a>
                @endforeach
            </div>
            <form method="POST" action="{{ route($category->type . '.update', $category->id) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <input type="hidden" name="locale" value="{{ $locale }}">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                @if (isset($parent_category) && $parent_category == true)
                                    <div class="form-group col-xl-6 col-12">
                                        <label for="inputStatus">上層類別</label>
                                        <select id="inputStatus" name="parent_id" class="form-control custom-select">
                                            @foreach (App\Models\Category::all() as $parent)
                                                @php
                                                    if ($parent->type !== 'projection_category' || $parent->removeable !== 0) {
                                                        continue;
                                                    }
                                                @endphp
                                                <option value="{{ $parent->id }}"
                                                    {{ $parent->id == $category->parent_id ? 'selected' : '' }}>
                                                    {{ $parent->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                {{-- 類別名稱 --}}
                                @include('categories.input.cateTitleInput')
                                {{-- 類別介紹 互動桌  --}}
                                @if ($category_type === 'desktop_category')
                                    @include('categories.input.cateDescriptionInput')
                                @endif
                                {{-- 上下架 --}}
                                @include('categories.input.cateStatusInput')
                                {{-- 封面圖片上傳及預覽 --}}
                                @if ($category_type !== 'projection_category')
                                    @include('categories.input.cateThumbnailInput')
                                @endif
                                {{-- 類別排序 --}}
                                @if ($category->removeable === 0 && $category_type === 'tvwall_category')
                                    <div style="display:none">
                                        @include('categories.input.cateSortInput')
                                    </div>
                                @else
                                    @include('categories.input.cateSortInput')
                                @endif
                                {{-- 互動桌 上傳三個影片 --}}
                                @if ($category_type === 'desktop_category')
                                    <div class="form-group col-xl-6 col-12 mt-3">
                                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                            data-target="#mediaUploadVideo" @click="handleOpenVideoMedia">瀏覽影片媒體</button>
                                    </div>
                                    <!-- 使用者看到的已經選擇的影片媒體 -->
                                    @include('posts.input.attachments', ['mediaType' => 'video'])
                                @endif
                                <!-- 儲存時送到後端的mediaID -->
                                @include('posts.input.mediaInput')

                                {{-- 電視牆的各國主題 選擇多圖 --}}
                                @if ($category_type === 'tvwall_category' && $category->removeable === 0)
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                            data-target="#mediaUploadPic" @click="handleOpenPictureMedia">瀏覽圖片媒體</button>
                                    </div>
                                    <!-- 使用者看到的已經選擇的圖片媒體 -->
                                    @include('posts.input.attachments', ['mediaType' => 'picture'])
                                @endif
                                @if ($category_type === 'projection_category')
                                @elseif ($category_type === 'tvwall_category' && $category->removeable === 0)
                                @else
                                    <div class="d-flex justify-content-between align-items-center mt-3">
                                        <p>內容排序</p>
                                        {{-- 排序按鈕 --}}
                                        @include('components.sortButton')
                                    </div>
                                    @include('categories.partials.postsCard')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 mb-3">
                        <a href="{{ route($category->type . '.index') }}" class="btn btn-secondary">取消</a>
                        @if (in_array(
                                'admin',
                                Auth::user()->getRoleNames()->toArray()))
                            <input type="submit" value="更新類別" class="btn btn-success float-right">
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <!-- 選擇媒體的modal -->
        @include('posts.input.medioUpload', ['mediaType' => 'mp4', 'modalId' => 'mediaUploadVideo'])
        @include('posts.input.medioUpload', ['mediaType' => 'jpg', 'modalId' => 'mediaUploadPic'])
    </div>
@endsection

@include('categories.script.editscript')
