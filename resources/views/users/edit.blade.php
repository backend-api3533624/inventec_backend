@extends('adminlte::page')

@section('content_header')
    <h1>編輯使用者</h1>
@stop

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ route('users.update', $user->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-column">
                                    <div class="col-sm-6">
                                        <div class="form-group {{ empty($errors->first('status')) ? '' : 'has-error' }}">
                                            <label for="status">狀態</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="0" {{ $user->status == 0 ? 'selected' : '' }}>停用
                                                </option>
                                                <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>啟用
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group {{ empty($errors->first('username')) ? '' : 'has-error' }}">
                                            <label for="username">使用者名稱</label>
                                            <input type="text" name="username" class="form-control disabled"
                                                id="username" value="{{ $user->name }}" disabled="disabled" />
                                        </div>
                                        {{-- 上面disable不會被傳遞給後端 --}}
                                        <div class="form-group {{ empty($errors->first('username')) ? '' : 'has-error' }}"
                                            style='display:none'>
                                            <label for="username">使用者名稱</label>
                                            <input type="text" name="username" class="form-control disabled"
                                                id="username" value="{{ $user->name }}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group {{ empty($errors->first('email')) ? '' : 'has-error' }}">
                                            <label for="email">Email</label>
                                            <input type="text" name="email" class="form-control" id="email"
                                                value="{{ $user->email }}" disabled="disabled" />
                                        </div>
                                        {{-- 上面disable不會被傳遞給後端 --}}
                                        <div class="form-group {{ empty($errors->first('email')) ? '' : 'has-error' }}"
                                            style='display:none'>
                                            <label for="email">Email</label>
                                            <input type="text" name="email" class="form-control" id="email"
                                                value="{{ $user->email }}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group {{ empty($errors->first('password')) ? '' : 'has-error' }}">
                                            <label for="password">密碼</label>
                                            <input type="password" name="password" class="form-control" id="password"
                                                value="" placeholder="留白則不修改密碼" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">角色</h3>
                            </div>
                            <div class="card-body">
                                @foreach ($roles as $k => $role)
                                    <div class="form-check my-3">
                                        <input class="form-check-input" type="checkbox" name="roles[]"
                                            id="roles_{{ $k }}" value="{{ $role->name }}"
                                            {{ $user->hasRole($role->name) ? 'checked' : '' }}>
                                        <label class="form-check-label" for="roles_{{ $k }}">
                                            {{ $role->name }}
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mb-3">儲存</button>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
