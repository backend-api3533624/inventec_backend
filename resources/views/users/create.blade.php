@extends('adminlte::page')

@section('content_header')
    <h1>建立使用者</h1>
@stop

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('users.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-column">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="status">狀態</label>
                                            <select class="form-control @error('status') is-invalid @enderror"
                                                name="status" id="status">
                                                <option value="1" {{ old('status') == '1' ? 'selected' : '' }}>啟用
                                                </option>
                                                <option value="0" {{ old('status') == '0' ? 'selected' : '' }}>停用
                                                </option>
                                            </select>
                                            @error('status')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name">使用者名稱*</label>
                                            <input type="text"
                                                class="form-control @error('username') is-invalid @enderror" name="username"
                                                placeholder="" id="username" value="{{ old('username') }}" required>
                                            @error('username')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email*</label>
                                            <input type="text" class="form-control @error('email') is-invalid @enderror"
                                                name="email" id="email" value="{{ old('email') }}" placeholder=""
                                                required>
                                            @error('email')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password">密碼*</label>
                                            <input type="password"
                                                class="form-control @error('password') is-invalid @enderror" id="password"
                                                placeholder="" name="password" required>
                                            @error('password')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">角色</h3>
                            </div>
                            <div class="card-body">
                                @foreach ($roles as $role)
                                    <div class="form-check my-3">
                                        @if (!empty(old('roles')) && in_array($role->id, old('roles')))
                                            <input name="roles[]" type="checkbox" id="role{{ $role->id }}"
                                                class="form-check-input" value="{{ $role->name }}" checked>
                                        @else
                                            <input name="roles[]" type="checkbox" id="role{{ $role->id }}"
                                                class="form-check-input" value="{{ $role->name }}">
                                        @endif
                                        <label for="role{{ $role->id }}" class="form-check-label">
                                            {{ $role->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mb-3">建立</button>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
