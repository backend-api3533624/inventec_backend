@extends('adminlte::page')

@section('content_header')
    <h1>使用者清單</h1>
@stop

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-3 right">
                <div class="mr-3">
                    <a href="{{ route('users.create') }}" class="btn btn-success">建立使用者</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">使用者清單
                            <div class="card-tools">
                                <form action="{{ route('users.index') }}" method="GET">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="filter" class="form-control float-right"
                                            placeholder="Search" value="{{ request()->input('filter') }}">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-light table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="col-sm-1">#</th>
                                        <th scope="col">名稱</th>
                                        <th scope="col">Email</th>
                                        <th scope="col" class="col-sm-1">狀態</th>
                                        <th scope="col" class="col-sm-2">操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <th scope="row">
                                                {{ $user->id }}
                                            </th>
                                            <td>
                                                {{ $user->name }}
                                            </td>
                                            <td>
                                                <a href="{{ route('users.edit', $user->id) }}">
                                                    {{ $user->email }}</a>
                                            </td>
                                            <td>
                                                @if ($user->status === 0)
                                                    <div
                                                        style="background-color: rgb(190, 190, 190); width: 18px; height: 18px; border-radius: 50%;">
                                                    </div>
                                                @elseif ($user->status === 1)
                                                    <div
                                                        style="background-color: green; width: 18px; height: 18px; border-radius: 50%;">
                                                    </div>
                                                @else
                                                    <p>異常</p>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('users.edit', $user->id) }}"
                                                    class="btn btn-info btn-sm"><i class="fa fas fa-pencil-alt">
                                                    </i> 編輯</a>
                                            </td>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="col">
                                <div class="pagination pagination-sm">
                                    <!-- pagination  -->
                                    {{ $users->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('plugins.BsCustomFileInput', true)

@section('js')
    <script>
        $(document).ready(function() {
            bsCustomFileInput.init()
        })
    </script>
@endsection
