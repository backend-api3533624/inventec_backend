@if ($errors->any())
    @foreach ($errors->all() as $error)
        <x-adminlte-alert theme="warning" title="Warning">
            {{ $error }}
        </x-adminlte-alert>
    @endforeach
@endif
@if (session()->has('success'))
    <x-adminlte-alert theme="success" title="Success">
        {{ session('success') }}
    </x-adminlte-alert>
@endif
@if (session()->has('error'))
    <x-adminlte-alert theme="danger" title="Danger">
        {{ session('error') }}
    </x-adminlte-alert>
@endif

@section('plugins.BsCustomFileInput', true)
