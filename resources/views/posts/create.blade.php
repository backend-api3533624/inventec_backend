@extends('adminlte::page')

@section('meta_tags')
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
@endsection

@section('content_header')
    @php
        $routeLabels = [
            'tvwall_post' => '電視牆／建立內容',
            'theater_post' => '動態劇院／建立內容',
            'desktop_post' => '互動桌／建立內容',
            'cockpit_post' => '智慧座艙／建立內容',
            'projection_post' => '光雕投影牆／建立內容',
            'screen_post' => '透明屏／建立內容',
            'fans3d_post' => '3D風扇／建立內容',
        ];
        $defaultLabel = '建立內容';
    @endphp

    <h1>
        {{ $routeLabels[$route] ?? $defaultLabel }}
    </h1>
@stop


@section('content')
    @include('module.alerts')
    <div id="postEditApp">
        <div id="getIdForVue" style="display:none">0</div>
        <div class="content">
            <div class="container-fluid">
                <form method="POST" action="{{ route($route . '.store') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="post_type" value="{{ $post_type }}">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-primary">
                                <div class="card-body">
                                    <div class="form-group col-xl-6 col-12">
                                        <label for="locale">語系</label>
                                        <select id="locale" class="form-control" name="locale">
                                            @php
                                                $app_locales = App\Models\Locale::where('status', 1)->get();
                                            @endphp
                                            @foreach ($app_locales as $app_locale)
                                                <option value="{{ $app_locale->code }}">{{ $app_locale->chinese }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if (in_array($route, ['desktop_post']))
                                        {{-- 互動桌 多選所屬類別 --}}
                                        @include('posts.input.localeInputMultiple')
                                    @endif
                                    <!-- 標題 -->
                                    @include('posts.input.titleInput')
                                    {{-- 摘要 --}}
                                    {{-- 在route === 'desktop_post' 時才顯示 --}}
                                    @if ($route === 'desktop_post')
                                        @include('posts.input.descriptionInput')
                                    @endif
                                    <!-- 描述 -->
                                    {{-- 在route !== 'fans3d_post' 時才顯示 --}}
                                    @unless ($route === 'fans3d_post')
                                        @include('posts.input.contentInput')
                                    @endunless
                                    {{-- 所屬類別 --}}
                                    @isset($categories)
                                        @unless (in_array($route, ['desktop_post']))
                                            @include('posts.input.localeInput')
                                        @endunless
                                    @endisset
                                    <!-- 上下架 -->
                                    @include('posts.input.statusInput')
                                    <div class="form-group col-xl-6 col-12">
                                        <label for="upload_file">封面圖片</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="upload_file"
                                                    name="thumbnail" accept="image/jpeg">
                                                <label class="custom-file-label" for="upload_file">選擇檔案</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3 col-xl-6 col-12 d-flex">
                                        <div class="ml-2 mb-3" style="width: 192px; height: 108px;">
                                            <!-- 圖片預覽 -->
                                            <img id="preview_image"
                                                src="{{ !empty($post) && !empty($post->getTranslation('thumbnail', $locale, 0)) ? asset('storage/' . $post->getTranslation('thumbnail', $locale, 0)) : asset('images/default-image.svg') }}"
                                                alt="Preview" style="width: 192px; height: 108px; object-fit:cover;">
                                        </div>
                                        <div class="d-flex flex-column justify-content-center ml-2"
                                            style="width: 140px; height: 108px;">
                                            <h6 class="text-secondary small">上傳檔案限制</h6>
                                            <span class="text-secondary small">圖片格式：jpg<br>尺寸：建議
                                                1920*1080<br>檔案大小：5MB</span>
                                        </div>
                                    </div>
                                    @if (in_array($route, ['theater_post', 'screen_post']))
                                        @isset($properties)
                                            @foreach ($properties as $key => $property)
                                                <div class="form-group col-xl-6 col-12">
                                                    <label for="locale">屬性 - {{ $property->description }}</label>
                                                    <select id="locale" class="form-control"
                                                        name="post_meta[{{ $key }}]">
                                                        <option value="">無</option>
                                                        @foreach ($post_metas[$key] as $index => $desc)
                                                            <option value="{{ $index }}">
                                                                {{ $desc }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endforeach
                                        @endisset
                                    @endif

                                    <!-- 儲存時送到後端的mediaID -->
                                    @include('posts.input.mediaInput')
                                    <!-- 新增媒體 -->
                                    <button class="btn btn-info mb-3" type="button" data-toggle="collapse"
                                        data-target="#collapseExample" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        新增媒體
                                    </button>
                                    <div class="collapse" id="collapseExample">
                                        @include('media.upload')
                                    </div>
                                    <!-- 瀏覽媒體button -->
                                    @if (in_array($route, ['tvwall_post', 'theater_post', 'cockpit_post', 'desktop_post', 'screen_post', 'fans3d_post']))
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                                data-target="#mediaUploadVideo"
                                                @click="handleOpenVideoMedia">瀏覽影片媒體</button>
                                        </div>
                                        <!-- 使用者看到的已經選擇的影片媒體 -->
                                        @include('posts.input.attachments', ['mediaType' => 'video'])
                                    @endif
                                    @if (in_array($route, ['desktop_post', 'projection_post', 'screen_post']))
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="button" data-toggle="modal"
                                                data-target="#mediaUploadPic"
                                                @click="handleOpenPictureMedia">瀏覽圖片媒體</button>
                                        </div>
                                        <!-- 使用者看到的已經選擇的圖片媒體 -->
                                        @include('posts.input.attachments', ['mediaType' => 'picture'])
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 mb-3">
                            <a href="{{ route($route . '.index') }}" class="btn btn-secondary">取消</a>
                            <input type="submit" value="建立" class="btn btn-success float-right">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- 選擇媒體的modal -->
        @include('posts.input.medioUpload', ['mediaType' => 'mp4', 'modalId' => 'mediaUploadVideo'])
        @include('posts.input.medioUpload', ['mediaType' => 'jpg', 'modalId' => 'mediaUploadPic'])
    </div>
@endsection

@include('posts.script.editscript')
