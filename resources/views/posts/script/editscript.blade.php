@php
    if (empty($mediaFiles)) {
        $mediaFiles = [];
    }
@endphp
@section('js')
    <script>
        $(document).ready(function() {
            bsCustomFileInput.init();

            $('#mySelect').select2();

            let formChanged = false;

            // 監聽表單元素的更改事件
            $('form').on('change', ':input', function() {
                formChanged = true;
            });

            // 在表單提交之前檢查是否有更動
            $('form').submit(function() {
                formChanged = false;
            });

            // 在用戶離開頁面之前顯示提示
            $(window).on('beforeunload', function() {
                if (formChanged) {
                    return "您有未保存的更改，確定要離開嗎？";
                }
            });
            // 影片的排序功能
            $('#start_sort_function_video').click(function() {
                event.preventDefault();
                $('.ui-sortable-handle-video').append('<i class="fas fa-ellipsis-v"></i>');
                $('.ui-sortable-handle-video').append('<i class="fas fa-ellipsis-v"></i>');
                $(this).hide();
                $('#store_sort_video').show();
            });
            // 儲存排序
            $("#store_sort_video").click(function() {
                event.preventDefault();
                let numbers = [];
                $('.todo-list.ui-sortable.videoAttachmentTbody .mediaId').each(function() {
                    let number = $(this).text().trim();
                    numbers.push(number);
                });
                $('.ui-sortable-handle-video').find('i.fa-ellipsis-v').remove();
                $(this).hide();
                $('#start_sort_function_video').show();

                const formData = new FormData();

                formData.append('type', `{{ $route }}`);
                formData.append('item_sort', numbers.join(','));

                const videoMediaIds = document.querySelector(".videoMediaIds")
                videoMediaIds.innerHTML = ''
                for (var i = 0; i < numbers.length; i++) {
                    // Create a new input element
                    var inputElement = document.createElement('input');

                    // Set attributes for the input element
                    inputElement.setAttribute('id', 'media');
                    inputElement.setAttribute('class', 'form-control');
                    inputElement.setAttribute('type', 'text');
                    inputElement.setAttribute('name', 'media_ids[]');
                    inputElement.setAttribute('value', numbers[i]); // Set the value from the array
                    inputElement.setAttribute('style', 'display:none');

                    // Create a new div element
                    var divElement = document.createElement('div');
                    divElement.setAttribute('class', 'form-group col-xl-6 col-12');

                    // Append the input element to the div element
                    divElement.appendChild(inputElement);

                    // Append the div element to the parent div
                    videoMediaIds.appendChild(divElement);
                }

                $.ajax({
                    url: `/api/post/{{ !empty($post) ? $post->id : 0 }}/media/sort`,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    xhrFields: {
                        withCredentials: true // Include cookies for CSRF (SPA)
                    },
                    success: function(response) {
                        console.log(response); // Handle the response from the server
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown); // Handle any errors
                    }
                });


            });

            // 圖片的排序功能
            $('#start_sort_function_pic').click(function() {
                event.preventDefault();
                $('.ui-sortable-handle-pic').append('<i class="fas fa-ellipsis-v"></i>');
                $('.ui-sortable-handle-pic').append('<i class="fas fa-ellipsis-v"></i>');
                $(this).hide();
                $('#store_sort_pic').show();
            });
            // 儲存排序
            $("#store_sort_pic").click(function() {
                event.preventDefault();
                let numbers = [];
                $('.todo-list.ui-sortable.pictureAttachmentTbody .mediaId').each(function() {
                    let number = $(this).text().trim();
                    numbers.push(number);
                });
                $('.ui-sortable-handle-pic').find('i.fa-ellipsis-v').remove();
                $(this).hide();
                $('#start_sort_function_pic').show();

                const formData = new FormData();

                formData.append('type', `{{ $route }}`);
                formData.append('item_sort', numbers.join(','));

                const pictureMediaIds = document.querySelector(".pictureMediaIds")
                pictureMediaIds.innerHTML = ''
                for (var i = 0; i < numbers.length; i++) {
                    // Create a new input element
                    var inputElement = document.createElement('input');

                    // Set attributes for the input element
                    inputElement.setAttribute('id', 'media');
                    inputElement.setAttribute('class', 'form-control');
                    inputElement.setAttribute('type', 'text');
                    inputElement.setAttribute('name', 'media_ids[]');
                    inputElement.setAttribute('value', numbers[i]); // Set the value from the array
                    inputElement.setAttribute('style', 'display:none');

                    // Create a new div element
                    var divElement = document.createElement('div');
                    divElement.setAttribute('class', 'form-group col-xl-6 col-12');

                    // Append the input element to the div element
                    divElement.appendChild(inputElement);

                    // Append the div element to the parent div
                    pictureMediaIds.appendChild(divElement);
                }

                $.ajax({
                    url: `/api/post/{{ !empty($post) ? $post->id : '0' }}/media/sort`,
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    xhrFields: {
                        withCredentials: true // Include cookies for CSRF (SPA)
                    },
                    success: function(response) {
                        console.log(response); // Handle the response from the server
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown); // Handle any errors
                    }
                });
            });

            $('#upload_file').on('change', function(e) {
                let files = e.target.files;

                // 檢查並限制檔案大小
                if (files.length > 0) {
                    let fileSize = files[0].size;

                    // 設定最大大小限制
                    let maxFileSize = 5 * 1024 * 1024;

                    // 如果檔案超過最大大小，提示用戶並清空檔案選擇框
                    if (fileSize > maxFileSize) {
                        alert('檔案大小超過限制。');
                        $(this).val('');
                        $(this).closest('.custom-file').find('.custom-file-label').text('Choose file');
                        return false;
                    }

                    // 顯示預覽圖片
                    showPreview(files[0]);
                } else {
                    // 如果未選擇檔案，重置預覽並顯示 "尚未上傳圖片"
                    resetPreview();
                }
            });

            function showPreview(file) {
                // 使用 FileReader 來讀取檔案並顯示預覽圖片
                let reader = new FileReader();

                reader.onload = function(e) {
                    // 更新預覽圖片的 src
                    $('#preview_image').attr('src', e.target.result);

                    // 顯示預覽圖片，隱藏 "尚未上傳圖片"
                    $('.upload-placeholder').hide();
                    $('#preview_image').show();
                };

                reader.readAsDataURL(file);
            }

            function resetPreview() {
                // 重置預覽圖片並顯示 "尚未上傳圖片"
                $('#preview_image').attr('src', '');
                $('.upload-placeholder').show();
                $('#preview_image').hide();
            }

            // Enable sortable feature
            $(".todo-list.ui-sortable").sortable({
                placeholder: "ui-state-highlight",
                handle: ".handle",
                update: function(event, ui) {
                    // Handle the new order
                    console.log("New order:", $(this).sortable("toArray"));
                }
            });
            $(".todo-list").disableSelection();
        });
    </script>
@endsection
