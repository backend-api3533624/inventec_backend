<div class="form-group col-xl-6 col-12">
    <label for="locale">選擇在哪個類別顯示</label>
    <select id="locale" class="form-control" name="category[]">
        <option value="">無分類</option>
        @foreach ($categories as $category)
            @if (!empty($category->children) && count($category->children) > 0)
                @foreach ($category->children as $item)
                    <option value="{{ $item->id }}"
                        {{ isset($post) && !empty($post->categories) && $post->categories->pluck('id')->first() == $item->id ? 'selected' : '' }}>
                        &nbsp;{{ $category->getTranslation('content', $locale, 1) . '-' . $item->getTranslation('content', $locale, 1) }}
                    </option>
                @endforeach
            @elseif($route !== 'projection_post')
                <option value="{{ $category->id }}"
                    {{ isset($post) && !empty($post->categories) && $post->categories->pluck('id')->first() == $category->id ? 'selected' : '' }}>
                    {{ $category->getTranslation('content', $locale, 1) }}</option>
            @endif
        @endforeach
    </select>
</div>
