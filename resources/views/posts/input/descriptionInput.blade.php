<div class="form-group col-xl-6 col-12">
    <label for="摘要">摘要*</label>
    <textarea id="description" class="form-control" name="description" rows="3" required>{{ !empty($post) ? $post->getTranslation('description', $locale, 0) : '' }}</textarea>
    <small id="emailHelp" class="form-text text-muted">多語系</small>
</div>
