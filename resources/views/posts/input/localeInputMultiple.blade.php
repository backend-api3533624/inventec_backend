<div class="form-group col-xl-6 col-12">
    <label for="mySelect">選擇所屬類別:</label>
    <select id="mySelect" class="form-control select2" multiple name="category[]">
        @foreach ($categories as $category)
            <option value="{{ $category->id }}"
                {{ isset($post) && !empty($post->categories) && in_array($category->id, $post->categories->pluck('id')->toArray()) ? 'selected' : '' }}>
                {{ $category->getTranslation('content', 'zh_cht', 0) }}</option>
        @endforeach
    </select>
</div>

@section('css')
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #ffffff;
            background-color: #0d6efd;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #ffffff;
        }
    </style>
@endsection
