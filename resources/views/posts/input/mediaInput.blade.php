<div class="idsForUpdate">
    @php
        $mediaType = !empty($post) ? 'post' : 'category';
        $mediaData = !empty($post) ? $post->medias ?? [] : $category->medias ?? [];
    @endphp

    <div class="videoMediaIds" ref="videoMediaIds">
        @foreach ($mediaData as $media)
            @if (
                ($mediaType === 'post' && $media->type === 'video/mp4') ||
                    ($mediaType === 'category' && $media->type === 'video/mp4'))
                <div class="form-group col-xl-6 col-12" data-media-id="{{ $media->id }}">
                    <input id="media" class="form-control" type="text" name="media_ids[]" value="{{ $media->id }}"
                        style="display:none">
                </div>
            @endif
        @endforeach
        <div class="form-group col-xl-6 col-12" data-media-id="">
            <input id="media" class="form-control" type="text" name="media_ids[]" value=""
                style="display:none">
        </div>
    </div>

    <div class="pictureMediaIds" ref="pictureMediaIds">
        @foreach ($mediaData as $media)
            @if (
                ($mediaType === 'post' && $media->type === 'image/jpeg') ||
                    ($mediaType === 'category' && $media->type === 'image/jpeg'))
                <div class="form-group col-xl-6 col-12" data-media-id="{{ $media->id }}">
                    <input id="media" class="form-control" type="text" name="media_ids[]"
                        value="{{ $media->id }}" style="display:none">
                </div>
            @endif
        @endforeach
        <div class="form-group col-xl-6 col-12" data-media-id="">
            <input id="media" class="form-control" type="text" name="media_ids[]" value=""
                style="display:none">
        </div>
    </div>
</div>
