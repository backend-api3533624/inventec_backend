<div class="form-group col-xl-6 col-12">
    <label for="status">上下架狀態</label>
    <div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="customSwitch1"
            {{ !empty($post) && $post->status ? 'checked' : '' }} name="status">
        <label class="custom-control-label" for="customSwitch1"></label>
    </div>
</div>
