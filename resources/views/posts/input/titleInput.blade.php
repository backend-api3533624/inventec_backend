<div class="form-group col-xl-6 col-12">
    <label for="title">標題*</label>
    <input id="title" class="form-control" type="text" name="title"
        value="{{ !empty($post) ? $post->getTranslation('title', $locale, 0) : '' }}" required>
    <small id="emailHelp" class="form-text text-muted">多語系</small>
</div>
