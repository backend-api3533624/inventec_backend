<!-- 選擇媒體的modal -->
<div class="modal fade" id='{{ $modalId }}' tabindex="-1" role="dialog" aria-labelledby="medoaUploadLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h5 class="modal-title">從媒體庫選擇</h5>
            </div>
            <div class="modal-body">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary">
                                <div class="card-body">
                                    <table class="table table-light">
                                        <thead>
                                            <tr>
                                                <th>編號</th>
                                                <th>縮圖</th>
                                                <th>名稱</th>
                                                <th>選擇</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($mediaType === 'mp4')
                                                <tr v-for="video in videoMedia" :key="video.id">
                                                    <th>@{{ video.id }}</th>
                                                    <td>
                                                        <img :src="video.thumbnail" class="thumbnail"
                                                            style="width: 192px; height: 100px; object-fit: cover;">
                                                    </td>
                                                    <td>@{{ video.name }}</td>
                                                    <td>
                                                        @if (in_array($route, ['tvwall_post', 'theater_post', 'cockpit_post', 'fans3d_post']))
                                                            <input type="radio"
                                                                name="{{ $mediaType === 'mp4' ? 'selectedVideo' : 'selectedPic' }}"
                                                                @change="handleCheckRadioChange($event,video,'mp4')"
                                                                :checked="isVideoSelected(video.id, 'mp4')">
                                                        @elseif(in_array($route, ['desktop_post', 'projection_post', 'screen_post', 'desktop_category']))
                                                            <input type="checkbox"
                                                                name="{{ $mediaType === 'mp4' ? 'selectedVideo' : 'selectedPic' }}"
                                                                @change="handleCheckBoxChange($event,video,'mp4')"
                                                                :checked="isVideoSelected(video.id, 'mp4')">
                                                        @endif
                                                    </td>
                                                </tr>
                                            @elseif($mediaType === 'jpg')
                                                <tr v-for="video in pictureMedia" :key="video.id">
                                                    <th>@{{ video.id }}</th>
                                                    <td>
                                                        <img :src="video.url" class="thumbnail"
                                                            style="width: 192px; height: 100px; object-fit: cover;">
                                                    </td>
                                                    <td>@{{ video.name }}</td>
                                                    <td>
                                                        @if (in_array($route, ['tvwall_post', 'theater_post', 'cockpit_post']))
                                                            <input type="radio"
                                                                name="{{ $mediaType === 'mp4' ? 'selectedVideo' : 'selectedPic' }}"
                                                                @change="handleCheckRadioChange($event,video,'jpg')"
                                                                :checked="isVideoSelected(video.id, 'jpg')">
                                                        @elseif(in_array($route, ['desktop_post', 'projection_post', 'screen_post', 'tvwall_category']))
                                                            <input type="checkbox"
                                                                name="{{ $mediaType === 'mp4' ? 'selectedVideo' : 'selectedPic' }}"
                                                                @change="handleCheckBoxChange($event,video,'jpg')"
                                                                :checked="isVideoSelected(video.id, 'jpg')">
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            {{-- pagination --}}
            <div class="d-flex justify-content-center">
                @if ($mediaType === 'mp4')
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <!-- Previous page link -->
                            <li v-if="videoPagination.currentPage !== 1" class="page-item">
                                <a class="page-link" @click="changePage(videoPagination.currentPage - 1, 'mp4')">
                                    <span>&laquo;</span>
                                </a>
                            </li>

                            <!-- Page links -->
                            <li v-for="number in videoPagination.lastPage" :key="number"
                                :class="{ 'page-item': true, 'active': number === videoPagination.currentPage }">
                                <a class="page-link" @click="changePage(number, 'mp4')">
                                    <span>@{{ number }}</span>
                                </a>
                            </li>

                            <!-- Next page link -->
                            <li v-if="videoPagination.currentPage !== videoPagination.lastPage" class="page-item">
                                <a class="page-link" @click="changePage(videoPagination.currentPage + 1, 'mp4')">
                                    <span>&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                @elseif($mediaType === 'jpg')
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <!-- Previous page link -->
                            <li v-if="picturePagination.currentPage !== 1" class="page-item">
                                <a class="page-link" @click="changePage(picturePagination.currentPage - 1, 'jpg')">
                                    <span>&laquo;</span>
                                </a>
                            </li>

                            <!-- Page links -->
                            <li v-for="number in picturePagination.lastPage" :key="number"
                                :class="{ 'page-item': true, 'active': number === picturePagination.currentPage }">
                                <a class="page-link" @click="changePage(number, 'jpg')">
                                    <span>@{{ number }}</span>
                                </a>
                            </li>

                            <!-- Next page link -->
                            <li v-if="picturePagination.currentPage !== picturePagination.lastPage" class="page-item">
                                <a class="page-link" @click="changePage(picturePagination.currentPage + 1, 'jpg')">
                                    <span>&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="handleCancel">關閉</button>
                @if ($mediaType === 'mp4')
                    <button type="button" class="btn btn-info"
                        id="{{ $mediaType === 'mp4' ? 'saveSelectVideo' : 'saveSelectPic' }}" data-dismiss="modal"
                        @click="handleSave('mp4')">儲存</button>
                @elseif($mediaType === 'jpg')
                    <button type="button" class="btn btn-info"
                        id="{{ $mediaType === 'jpg' ? 'saveSelectVideo' : 'saveSelectPic' }}" data-dismiss="modal"
                        @click="handleSave('jpg')">儲存</button>
                @endif
            </div>
        </div>
    </div>
</div>
