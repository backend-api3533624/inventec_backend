<div class="attachments @if ($mediaType === 'video') videoAttachment @else pictureAttachment @endif">
    {{-- 影片 --}}
    @if ($mediaType === 'video')
        <div class="d-flex align-items-center justify-content-between mb-3">
            <span>已選影片</span>
            {{-- 影片為單選時，不顯示排序按紐 --}}
            @if (!in_array($route, ['tvwall_post', 'theater_post', 'cockpit_post', 'fans3d_post']))
                <div>
                    <a href="" class="btn btn-primary" id="start_sort_function_video">編輯排序</a>
                    <a href="" class="btn btn-warning" id="store_sort_video" style="display: none;">儲存排序</a>
                </div>
            @endif
        </div>
        @if (in_array($route, ['desktop_category']))
            <span class="text-danger">注意：請選擇三支影片。</span><br>
            <span class="text-danger">依序為 1.入場動畫、 2.運作動畫、 3.出場動畫</span>
        @endif
        <table class="table table-light">
            <thead>
                <tr>
                    <th style="width: 80px;">編號</th>
                    <th style="width: 192px;">縮圖</th>
                    <th>名稱</th>
                    <th style="width: 100px;">操作</th>
                </tr>
            </thead>
            {{-- post中使用的模板 --}}
            <tbody class="todo-list ui-sortable videoAttachmentTbody">
                @if (!empty($post))
                    @php
                        $postMediasJson = json_encode($post->medias);
                    @endphp
                    <div id="getPostMediasJson" data-json="{{ $postMediasJson }}"></div>
                    <tr v-for="media in selectedVideo">
                        <th class="mediaId" style="width: 80px;" :data-id="media.id">
                            @{{ media.id }}<span class="handle ui-sortable-handle-video">
                            </span></th>
                        </th>
                        <td style="width: 192px; height: 108px;">
                            <img :src="media.thumbnail ? media.thumbnail : 'images/default-image.svg'"
                                class="thumbnailPreview" style="width: 192px; height: 108px; object-fit: cover;">
                        </td>
                        <td class="mediaName">@{{ media.name }}</td>
                        <td>
                            <div class="btn btn-danger btn-sm" @click="handleDelete($event,media.id)"><i
                                    class="fas fa-trash"></i> 刪除</div>
                        </td>
                    </tr>
                    {{-- category中使用的 --}}
                @elseif (!empty($category))
                    @php
                        $categoryMediasJson = json_encode($category->medias);
                    @endphp
                    <div id="getCategoryMediasJson" data-json="{{ $categoryMediasJson }}"></div>
                    <tr v-for="media in selectedVideo">
                        <th class="mediaId" style="width: 80px;" :data-id="media.id">
                            @{{ media.id }}<span class="handle ui-sortable-handle-video">
                            </span></th>
                        </th>
                        <td style="width: 192px; height: 108px;">
                            <img :src="media.thumbnail ? media.thumbnail : 'images/default-image.svg'"
                                class="thumbnailPreview" style="width: 192px; height: 108px; object-fit: cover;">
                        </td>
                        <td class="mediaName">@{{ media.name }}</td>
                        <td>
                            <div class="btn btn-danger btn-sm" @click="handleDelete($event,media.id)"><i
                                    class="fas fa-trash"></i> 刪除</div>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
        {{-- 圖片 --}}
    @elseif($mediaType === 'picture')
        <div class="d-flex align-items-center justify-content-between mb-3">
            <span>已選圖片</span>
            <div>
                <a href="" class="btn btn-primary" id="start_sort_function_pic">編輯排序</a>
                <a href="" class="btn btn-warning" id="store_sort_pic" style="display: none;">儲存排序</a>
            </div>
        </div>
        <table class="table table-light">
            <thead>
                <tr>
                    <th style="width: 80px;">編號</th>
                    <th style="width: 192px;">縮圖</th>
                    <th>名稱</th>
                    <th style="width: 100px;">操作</th>
                </tr>
            </thead>
            <tbody class="todo-list ui-sortable pictureAttachmentTbody">
                {{-- post中使用的 --}}
                @if (!empty($post))
                    @php
                        $postMediasJson = json_encode($post->medias);
                    @endphp
                    <div id="getPostMediasJson" data-json="{{ $postMediasJson }}"></div>
                    <tr v-for="media in selectedPicture">
                        <th class="mediaId" style="width: 80px;" :data-id="media.id">
                            @{{ media.id }}<span class="handle ui-sortable-handle-pic">
                            </span></th>
                        </th>
                        <td style="width: 192px; height: 108px;">
                            <img :src="media.file_path ? media.file_path : media.url" class="thumbnailPreview"
                                style="width: 192px; height: 108px; object-fit: cover;">
                        </td>
                        <td class="mediaName">@{{ media.name }}</td>
                        <td>
                            <div class="btn btn-danger btn-sm" @click="handleDelete($event,media.id)"><i
                                    class="fas fa-trash"></i> 刪除</div>
                        </td>
                    </tr>
                    {{-- category中使用的 --}}
                @elseif(!empty($category))
                    @php
                        $categoryMediasJson = json_encode($category->medias);
                    @endphp
                    <div id="getCategoryMediasJson" data-json="{{ $categoryMediasJson }}"></div>
                    <tr v-for="media in selectedPicture">
                        <th class="mediaId" style="width: 80px;" :data-id="media.id">
                            @{{ media.id }}<span class="handle ui-sortable-handle-pic">
                            </span></th>
                        </th>
                        <td style="width: 192px; height: 108px;">
                            <img :src="media.file_path ? media.file_path : media.url" class="thumbnailPreview"
                                style="width: 192px; height: 108px; object-fit: cover;">
                        </td>
                        <td class="mediaName">@{{ media.name }}</td>
                        <td>
                            <div class="btn btn-danger btn-sm" @click="handleDelete($event,media.id)"><i
                                    class="fas fa-trash"></i> 刪除</div>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    @endif
</div>
