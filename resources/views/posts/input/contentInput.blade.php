<div class="form-group col-xl-6 col-12">
    <label for="content">描述*</label>
    <textarea id="content" class="form-control" name="content" rows="3" required>{{ !empty($post) ? $post->getTranslation('content', $locale, 0) : '' }}</textarea>
    <small id="emailHelp" class="form-text text-muted">多語系</small>
</div>
