@extends('adminlte::page')

@section('content_header')
    @php
        $routeLabels = [
            'tvwall_post' => '電視牆／內容清單',
            'theater_post' => '動態劇院／內容清單',
            'desktop_post' => '互動桌／內容清單',
            'cockpit_post' => '智慧座艙／內容清單',
            'projection_post' => '光雕投影牆／內容清單',
            'screen_post' => '透明屏／內容清單',
            'fans3d_post' => '3D風扇／內容清單',
        ];
        $defaultLabel = '內容清單';
    @endphp

    <h1>
        {{ $routeLabels[$route] ?? $defaultLabel }}
    </h1>
@stop

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-3 right">
                <div class="mr-3">
                    <a href="{{ route($route . '.create') }}" class="btn btn-success"><i class="fa fa-solid fa-plus"></i>
                        建立內容</a>
                </div>
                @if ($route === 'screen_post')
                    @if (in_array(
                            'admin',
                            Auth::user()->getRoleNames()->toArray()))
                        @include('components.sortButton')
                    @endif
                @endif
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">內容清單
                            <div class="card-tools">
                                @if (0)
                                    <form action="{{ route($route . '.index') }}" method="GET">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="filter" class="form-control float-right"
                                                placeholder="Search" value="{{ request()->input('filter') }}">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <div>
                                <div class="d-flex w-100 bg-light align-items-center" style="height: 40px">
                                    <div style="width: 80px" class="pl-3"></div>
                                    <div class="flex-fill" style="min-width:180px">標題</div>
                                    @if ($route === 'theater_post')
                                        <div style="width: 80px">類型</div>
                                    @elseif ($route === 'screen_post')
                                        <div style="width: 80px">屏幕</div>
                                    @endif
                                    <div style="width: 80px">狀態</div>
                                    <div style="width: 180px">操作</div>
                                </div>
                                <div class="todo-list ui-sortable">
                                    @foreach ($posts as $post)
                                        <div class="d-flex w-100 align-items-center" style="min-height: 52px">
                                            <div scope="row" style="width: 80px" class='category_id'>
                                                <span class="handle ui-sortable-handle">
                                                </span>
                                                <div style="display:none">{{ $post->id }}</div>
                                            </div>
                                            <div class="flex-fill" style="min-width:180px">
                                                <a
                                                    href="{{ route($route . '.edit', $post->id) }}">{{ $post->getTranslation('title', 'zh_cht') }}</a>
                                            </div>
                                            @if ($route === 'theater_post')
                                                <div style="width: 80px">
                                                    {{ optional($post->metas->where('meta_key', 'theater_video_dimension')->first())->meta_value }}
                                                </div>
                                            @elseif ($route === 'screen_post')
                                                <div style="width: 80px">
                                                    {{ optional($post->metas->where('meta_key', 'screen_position')->first())->meta_value }}
                                                </div>
                                            @endif
                                            <div style="width: 80px">
                                                @if ($post->status)
                                                    上架
                                                @else
                                                    下架
                                                @endif
                                            </div>
                                            <div style="width: 180px">
                                                @if (in_array(
                                                        'admin',
                                                        Auth::user()->getRoleNames()->toArray()))
                                                    <form action="{{ route($route . '.destroy', $post) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a href="{{ route($route . '.edit', $post->id) }}"
                                                            class="btn btn-info btn-sm"><i class="fa fas fa-pencil-alt">
                                                            </i> 編輯</a>

                                                        <button class="btn btn-danger btn-sm" type="submit"
                                                            onclick="return confirm('確定要刪除這個類別嗎？');"><i
                                                                class="fas fa-trash">
                                                            </i> 刪除</button>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col">
                                <div class="pagination pagination-sm">
                                    <!-- pagination  -->
                                    {{ $posts->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            let screenCategoryId = ''
            const route = "{{ $route }}"

            @isset($screen_category)
                screenCategoryId = '{{ $screen_category->id }}'
            @endisset

            // 開啟排序功能
            $('#start_sort_function').click(function() {
                event.preventDefault();
                $('.ui-sortable-handle').append('<i class="fas fa-ellipsis-v"></i>');
                $('.ui-sortable-handle').append('<i class="fas fa-ellipsis-v"></i>');
                $(this).hide();
                $('#store_sort').show();
            });
            // 儲存排序
            $("#store_sort").click(function() {
                event.preventDefault();
                let numbers = [];
                $('.todo-list.ui-sortable .category_id').each(function() {
                    let number = $(this).text().trim();
                    numbers.push(number);
                });
                $('.ui-sortable-handle').find('i.fa-ellipsis-v').remove();
                $(this).hide();
                $('#start_sort_function').show();

                let formData = new FormData();
                formData.append('type', route);
                formData.append('item_sort', numbers.join(','));

                $.ajax({
                    url: `/api/category/${screenCategoryId}/post/sort`, // Change to your server script
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    xhrFields: {
                        withCredentials: true // Include cookies for CSRF (SPA)
                    },
                    success: function(response) {
                        console.log(response); // Handle the response from the server
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown); // Handle any errors
                    }
                });
            });

            // Enable sortable feature
            $(".todo-list.ui-sortable").sortable({
                placeholder: "ui-state-highlight",
                handle: ".handle",
                update: function(event, ui) {
                    console.log("New order:", $(this).sortable("toArray"));
                }
            });
            $(".todo-list").disableSelection();
        })
    </script>
@endsection
