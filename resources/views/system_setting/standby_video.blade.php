@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        @if ($route === 'tvwall_standby_video.update')
            電視牆／待機畫面
        @elseif($route === 'theater_standby_video.update')
            動態劇院／待機畫面
        @elseif($route === 'desktop_standby_video.update')
            互動桌／待機畫面
        @elseif($route === 'cockpit_standby_video.update')
            智慧座艙／待機畫面
        @elseif($route === 'screen_standby_video.update')
            透明屏／待機畫面
        @elseif($route === 'fans3d_standby_video.update')
            3D風扇／待機畫面
        @else
            待機畫面
        @endif
    </h1>
@stop

@section('content')
    @include('module.alerts')
    <section class="content">
        <div class="container-fluid">
            @if (in_array('admin', Auth::user()->getRoleNames()->toArray()))
                <div class="row">
                    <div class="col-xl-6 col-md-8">
                        <div class="card card-primary">
                            <div class="card-header">上傳檔案</div>
                            <form action="{{ route($route) }}" method="post" enctype="multipart/form-data">
                                <div class="card-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="upload_file">檔案</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="upload_file"
                                                    name="file" accept="image/jpeg, video/mp4">
                                                <label class="custom-file-label" for="upload_file">選擇檔案</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-md-3">
                                            <h6 class="text-secondary">上傳檔案限制</h5>
                                        </div>
                                        <!-- 建議檔案大小 -->
                                        @include('system_setting.sizeRecommend')
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">上傳</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-xl-6 col-md-8">
                    <div class="card card-primary">
                        <div class="card-header">目前待機畫面預覽</div>
                        <div class="card-body">
                            @if (!empty($video->media['file_path']))
                                @php
                                    // dump($video->media);
                                    $fileExtension = pathinfo($video->media['file_path'])['extension'];
                                @endphp

                                @if ($fileExtension == 'mp4')
                                    <div class="row col-12">
                                        <p>{{ $video->media['name'] }}</p>
                                    </div>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <video width="480" class="embed-responsive-item" controls loop muted>
                                            <source src="{{ asset('storage/' . $video->media['file_path']) }}"
                                                type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                @elseif(in_array($fileExtension, ['jpg', 'jpeg']))
                                    <div class="row col-12">
                                        <p>{{ $video->media['name'] }}</p>
                                    </div>
                                    <div class="row col-12">
                                        <img class="img-fluid" src="{{ asset('storage/' . $video->media['file_path']) }}"
                                            alt="tv-standby-image">
                                    </div>
                                @else
                                    <p>Unsupported file type</p>
                                @endif
                            @endif
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('plugins.BsCustomFileInput', true)

@section('js')
    <script>
        $(document).ready(function() {
            bsCustomFileInput.init();
        });

        $('#upload_file').on('change', function(e) {
            let files = e.target.files;

            // 檢查並限制檔案大小
            if (files.length > 0) {
                let fileSize = files[0].size;
                let maxFileSize = 0;

                // 根據檔案類型設定最大大小限制
                @if ($route === 'cockpit_standby_video.update')
                    maxFileSize = files[0].type === 'image/jpeg' ? 30 * 1024 * 1024 : 5 * 1024 * 1024 * 1024;
                @elseif ($route === 'fans3d_standby_video.update')
                    maxFileSize = files[0].type === 'image/jpeg' ? 15 * 1024 * 1024 : 30 * 1024 * 1024;
                @else
                    maxFileSize = files[0].type === 'image/jpeg' ? 15 * 1024 * 1024 : 1024 * 1024 * 1024;
                @endif

                // 如果檔案超過最大大小，提示用戶並清空檔案選擇框
                if (fileSize > maxFileSize) {
                    alert('檔案大小超過限制。');
                    $(this).val('');
                    $(this).closest('.custom-file').find('.custom-file-label').text('Choose file');
                    return false;
                }

                // 更新檔案選擇框的標籤
                $(this).closest('.custom-file').find('.custom-file-label').text(files[0].name);
            }
        });
    </script>
@endsection
