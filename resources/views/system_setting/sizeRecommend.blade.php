@php
    $formats = [
        'tvwall_standby_video.update' => ['size' => '2448x1560', 'imageLimit' => '15MB', 'videoLimit' => '1GB'],
        'theater_standby_video.update' => ['size' => '1920x720', 'imageLimit' => '15MB', 'videoLimit' => '1GB'],
        'desktop_standby_video.update' => ['size' => '2732x2048', 'imageLimit' => '15MB', 'videoLimit' => '1GB'],
        'cockpit_standby_video.update' => ['size' => '7680x1080', 'imageLimit' => '30MB', 'videoLimit' => '5GB'],
        'screen_standby_video.update' => ['size' => '1080x1920', 'imageLimit' => '15MB', 'videoLimit' => '1GB'],
        'fans3d_standby_video.update' => ['size' => '1080x1080', 'imageLimit' => '15MB', 'videoLimit' => '30MB'],
    ];
@endphp

@if (array_key_exists($route, $formats))
    <div class="col-6">
        <span
            class="text-secondary">圖片格式：jpg<br>建議尺寸：{{ $formats[$route]['size'] }}<br>檔案大小上限：{{ $formats[$route]['imageLimit'] }}</span>
    </div>
    <div class="col-6">
        <span
            class="text-secondary">影片格式：mp4<br>建議尺寸：{{ $formats[$route]['size'] }}<br>檔案大小上限：{{ $formats[$route]['videoLimit'] }}</span>
    </div>
@endif
