<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locales', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->string('name');
            $table->string('chinese');
            $table->string('english');
        });
        DB::statement('INSERT INTO `locales` (`id`, `code`, `status`, `name`, `chinese`, `english`) VALUES
            (1, "ar_xa", 0, "العربية", "阿拉伯文", "Arabic"),
            (2, "bg", 0, "български", "保加利亞文", "Bulgarian"),
            (3, "hr", 0, "hrvatska kultura", "克羅埃西亞文", "Croatian"),
            (4, "cs", 0, "Česky", "捷克文", "Czech"),
            (5, "da", 0, "dansk", "丹麥文", "Danish"),
            (6, "de", 0, "Devon", "德文", "German"),
            (7, "el", 0, "Ελληνικά", "希臘文", "Greek"),
            (8, "en", 1, "English", "英文", "English"),
            (9, "et", 0, "Eesti keel", "愛沙尼亞文", "Estonian"),
            (10, "es", 0, "Español", "西班牙文", "Spanish"),
            (11, "fi", 0, "suomalainen", "芬蘭文", "Finnish"),
            (12, "fr", 0, "Français", "法文", "French"),
            (13, "ga", 0, "Gaeilge", "愛爾蘭文", "Irish"),
            (14, "hi", 0, "भारतीय भाषा", "印度文", "Indian"),
            (15, "hu", 0, "magyar", "匈牙利文", "Hungarian"),
            (16, "he", 0, "עברית", "希伯來文", "Hebrew"),
            (17, "it", 0, "italiano", "義大利文", "Italian"),
            (18, "ja", 0, "日本語", "日文", "Japanese"),
            (19, "ko", 0, "한국어", "韓文", "Korean"),
            (20, "lv", 0, "Latviešu valoda", "拉脫維亞文", "Latvian"),
            (21, "lt", 0, "Lietuviškai", "立陶宛文", "Lithuanian language"),
            (22, "nl", 0, "Nederlands", "荷蘭文", "Dutch Language"),
            (23, "no", 0, "Norsk tekst", "挪威文", "Norwegian"),
            (24, "pl", 0, "Polski", "波蘭文", "Polish"),
            (25, "pt", 0, "Portugues", "葡萄牙文", "Portuguese"),
            (26, "sv", 0, "Svensk text", "瑞典文", "Swedish"),
            (27, "ro", 0, "românesc", "羅馬尼亞文", "Romanian"),
            (28, "ru", 0, "русский", "俄文", "Russian"),
            (29, "sr_cs", 0, "Српски", "塞爾維亞文", "Serbian"),
            (30, "sk", 0, "slovenský", "斯洛伐克文", "Slovak Wen"),
            (31, "sl", 0, "Slovenščina", "斯洛維尼亞文", "Slovene language"),
            (32, "th", 0, "ไทย", "泰文", "Thai language"),
            (33, "tr", 0, "Türk", "土耳其文", "Turkish"),
            (34, "uk_ua", 0, "Українська мова", "烏克蘭文", "Ukrainian"),
            (35, "vi", 0, "Tiếng Việt", "越南語", "Vietnamese"),
            (36, "zh_chs", 0, "简体中文", "簡體中文", "Simplified Chinese"),
            (37, "zh_cht", 1, "繁體中文", "繁體中文", "Traditional Chinese");'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locales');
    }
};
