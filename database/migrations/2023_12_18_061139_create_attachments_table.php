<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->unsignedBigInteger('media_id')->comment('素材');
            $table->string('related_type')->comment('關聯model');
            $table->unsignedBigInteger('related_id')->comment('關聯model.id');
            $table->string('related_locale')->nullable()->comment('語系');
            $table->integer('item_sort')->default(0)->comment('排序');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attachments');
    }
};
