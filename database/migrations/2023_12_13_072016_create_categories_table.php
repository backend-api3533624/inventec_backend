<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->json('name')->comment('多語系.名稱');
            $table->boolean('status')->default(1)->comment('狀態');
            $table->boolean('removeable')->default(1)->comment('可以刪除');
            $table->string('type')->comment('單元');
            $table->json('content')->nullable()->comment('多語系.內容');
            $table->json('description')->nullable()->comment('多語系.描述');
            $table->json('thumbnail')->nullable()->comment('多語系.縮圖');
            $table->tinyInteger('sort')->default(0)->comment('排序');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
