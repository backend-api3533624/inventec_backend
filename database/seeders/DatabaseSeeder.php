<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CategorySeeder::class,
            PostSeeder::class,
            SystemSettingSeeder::class,
        ]);
        // \App\Models\User::factory(10)->create();

        $admin = \App\Models\User::firstOrCreate([
            'email' => 'info@kingone-design.com',
        ], [
            'name' => 'admin',
            'password' => Hash::make('King0708'),
        ]);

        /**
         * Role, Permission
         */
        $role = Role::firstOrCreate([
            'name' => 'admin',
        ]);
        $permission = Permission::firstOrCreate([
            'name' => 'locale setting',
        ]);

        $role->givePermissionTo($permission);
        $role->syncPermissions($permission);
        $admin->syncRoles($role);

        $role = Role::firstOrCreate([
            'name' => 'editor',
        ]);
        $permission = Permission::firstOrCreate([
            'name' => 'edit post',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::firstOrCreate([
            'name' => 'edit categroy',
        ]);
        $role->givePermissionTo($permission);
    }
}
