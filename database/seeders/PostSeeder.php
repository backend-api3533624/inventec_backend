<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\Postmeta;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        $title = [
            'en' => $faker->sentence(3),
            'zh_cht' => $faker->sentence(3),
        ];
        $content = [
            'en' => $faker->text,
            'zh_cht' => $faker->text,
        ];
        /**
         * tvwall_post
         */
        Post::create([
            'name' => $faker->sentence(5),
            'title' => $title,
            'content' => $content,
            'status' => 0,
            'post_parent' => 0,
            'menu_order' => 0,
            'type' => 'video_post',
        ]);
        Post::create([
            'name' => $faker->sentence(5),
            'title' => $title,
            'content' => $content,
            'status' => 0,
            'post_parent' => 0,
            'menu_order' => 0,
            'type' => 'video_post',
        ]);
        Post::create([
            'name' => $faker->sentence(5),
            'title' => $title,
            'content' => $content,
            'status' => 0,
            'post_parent' => 0,
            'menu_order' => 0,
            'type' => 'video_post',
        ]);

        /**
         * award
         */

        Post::create([
            'name' => $faker->sentence(5),
            'title' => $title,
            'content' => $content,
            'status' => 0,
            'post_parent' => 0,
            'menu_order' => 0,
            'type' => 'award_post',
        ]);

        /**
         * patent
         */
        $post = Post::create([
            'name' => $faker->sentence(5),
            'title' => $title,
            'content' => $content,
            'status' => 0,
            'post_parent' => 0,
            'menu_order' => 0,
            'type' => 'patent_post',
        ]);

        Postmeta::create([
            'post_id' => $post->id,
            'meta_key' => 'screen_position',
            'meta_value' => 'left',
        ]);

        $post = Post::create([
            'name' => $faker->sentence(5),
            'title' => $title,
            'content' => $content,
            'status' => 0,
            'post_parent' => 0,
            'menu_order' => 0,
            'type' => 'desktop_post',
        ]);
    }
}
