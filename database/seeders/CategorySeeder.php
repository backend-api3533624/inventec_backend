<?php

namespace Database\Seeders;

use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        for ($i = 0; $i < 3; $i++) {
            $faker = Faker::create();
            $name = $faker->sentence;
            $content = [
                'en' => $faker->sentence,
                'zh_cht' => $faker->sentence,
            ];

            $description = [
                'en' => $faker->paragraph,
                'zh_cht' => $faker->paragraph,
            ];

            $thumbnail = [
                'en' => 'https://example.com/video.mp4',
                'zh_cht' => 'https://example.com/video.mp4',
            ];

            Category::create([
                'name' => $name,
                'type' => 'video_category',
                'content' => $content,
                'description' => $description,
                'thumbnail' => $thumbnail,
                'sort' => $i,
            ]);
        }

        $faker = Faker::create();
        Category::create([
            'name' => '各國主題',
            'status' => 1,
            'type' => 'video_category',
            'removeable' => 0,
            'content' => $content,
            'description' => $description,
            'thumbnail' => $thumbnail,
        ]);

        /**
         * award
         */
        Category::create([
            'name' => '產線A',
            'status' => 1,
            'type' => 'award_category',
            'removeable' => 1,
            'content' => [
                'en' => 'A1-Product line',
                'zh_cht' => 'A1-產線',
            ],
            'description' => '',
            'thumbnail' => null,
        ]);
        Category::create([
            'name' => '產線B',
            'status' => 1,
            'type' => 'award_category',
            'removeable' => 1,
            'content' => [
                'en' => 'B1-Product line',
                'zh_cht' => 'B1-產線',
            ],
            'description' => '',
            'thumbnail' => null,
        ]);

        /**
         * patent 
         */

        Category::create([
            'name' => 'Default',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'defualt',
                'zh_cht' => 'defualt',
            ],
            'description' => [
                'en' => 'defualt',
                'zh_cht' => 'defualt',
            ],
            'thumbnail' => [
                'en' => null,
                'zh_cht' => null,
            ],
        ]);
        $thumbnail = [
            'en' => 'images/Asia.svg',
        ];
        $category = Category::create([
            'name' => '亞洲',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'Asia',
                'zh_cht' => '亞洲',
            ],
            'description' => '',
            'thumbnail' => $thumbnail,
        ]);
        Category::create([
            'parent_id' => $category->id,
            'name' => '台灣',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 1,
            'content' => [
                'en' => 'Taiwan',
                'zh_cht' => '台灣',
            ],
            'description' => '台灣',
            'thumbnail' => null,
        ]);
        $thumbnail = [
            'en' => 'images/NorthAmerica.svg',
        ];
        Category::create([
            'name' => '北美洲',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'Northamerica',
                'zh_cht' => '北美洲',
            ],
            'description' => null,
            'thumbnail' => $thumbnail,
        ]);
        $thumbnail = [
            'en' => 'images/SouthAmerica.svg',
        ];
        Category::create([
            'name' => '南美洲',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'Southamerica',
                'zh_cht' => '南美洲',
            ],
            'description' => null,
            'thumbnail' => $thumbnail,
        ]);
        $thumbnail = [
            'en' => 'images/Africa.svg',
        ];
        Category::create([
            'name' => '非洲',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'Africa',
                'zh_cht' => '非洲',
            ],
            'description' => null,
            'thumbnail' => $thumbnail,
        ]);
        $thumbnail = [
            'en' => 'images/Australasia.svg',
        ];
        Category::create([
            'name' => '澳洲',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'Australia',
                'zh_cht' => '澳洲',
            ],
            'description' => null,
            'thumbnail' => $thumbnail,
        ]);
        $thumbnail = [
            'en' => 'images/Europe.svg',
        ];
        Category::create([
            'name' => '歐洲',
            'status' => 1,
            'type' => 'patent_category',
            'removeable' => 0,
            'content' => [
                'en' => 'Europe',
                'zh_cht' => '歐洲',
            ],
            'description' => null,
            'thumbnail' => $thumbnail,
        ]);
    }
}
