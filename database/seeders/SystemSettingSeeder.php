<?php

namespace Database\Seeders;

use App\Models\SystemSetting;
use Illuminate\Database\Seeder;

class SystemSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /**
         * 最後更新時間
         */
        SystemSetting::firstOrCreate([
            'setting_key' => 'tvwall_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'theater_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'desktop_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'cockpit_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'projection_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'screen_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'fans3d_last_edit_timestamp',
        ], [
            'setting_value' => date('Y-m-d H:i:s'),
        ]);

        /**
         * 待機畫面
         */
        SystemSetting::firstOrCreate([
            'setting_key' => 'tvwall_standby_video',
        ], [
            'setting_value' => '',
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'cockpit_standby_video',
        ], [
            'setting_value' => '',
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'theater_standby_video',
        ], [
            'setting_value' => '',
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'screen_standby_video',
        ], [
            'setting_value' => '',
        ]);

        /**
         * Desktop
         */
        SystemSetting::firstOrCreate([
            'description' => '類別多選',
            'setting_key' => 'desktop_post_multiple_category',
            'setting_value' => 1,
        ]);
        /**
         * theater
         */
        SystemSetting::firstOrCreate([
            'setting_key' => 'theater_video_dimension',
        ], [
            'description' => '影片類別',
            'setting_value' => [
                '2d' => '2D',
                '3d' => '3D',
            ],
        ]);

        /**
         * screen
         */
        SystemSetting::firstOrCreate([
            'setting_key' => 'screen_position',
        ], [
            'description' => '位置',
            'setting_value' => [
                'left' => '左',
                'right' => '右',
            ],
        ]);
        SystemSetting::firstOrCreate([
            'setting_key' => 'screen_device',
        ], [
            'description' => '裝置',
            'setting_value' => [
                'interactive' => '體驗',
                'monitor_169' => '16:9',
            ],
        ]);
    }
}
